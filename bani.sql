-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Jun 2021 pada 03.41
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bani`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `email`, `api_token`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$wbfkg9YPh2mzsIj3XZXuHOGUrP0e058H5pLb.QsFjWJwAAipKb64a', '$2y$10$H6q.DDZdJ2fxdr0JTk.26OYh4x.geQPpm9JuFCfy3ws1gLcswTjLK', '$2y$10$wbfkg9YPh2mzsIj3XZXuHOGUrP0e058H5pLb.QsFjWJwAAipKb64a', '2018-09-04 17:00:00', '2019-01-03 16:01:10'),
(2, 'Nur Kholis', 'admin2@gmail.com', '$2y$10$wbfkg9YPh2mzsIj3XZXuHOGUrP0e058H5pLb.QsFjWJwAAipKb64a', '$2y$10$wbfkg9YPh2mzsIj3XZXuHOGUrP0e058H5pLb.QsFjWJwAAipKb64a', '$2y$10$wbfkg9YPh2mzsIj3XZXuHOGUrP0e058H5pLb.QsFjWJwAAipKb64a', '2018-09-04 17:00:00', '2019-06-08 05:33:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `album`
--

CREATE TABLE `album` (
  `id_album` int(11) NOT NULL,
  `nama_album` varchar(200) NOT NULL,
  `cover_album` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `album`
--

INSERT INTO `album` (`id_album`, `nama_album`, `cover_album`) VALUES
(1, 'Halal bihalal Bani Sadinah 2016', '1559996271.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(11) NOT NULL,
  `id_bani` int(11) NOT NULL,
  `id_ayah` int(11) DEFAULT NULL,
  `id_ibu` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` text NOT NULL,
  `api_token` text NOT NULL,
  `nama` varchar(255) NOT NULL,
  `pasangan` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `gender` enum('L','P') NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `foto` text DEFAULT NULL,
  `generasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `id_bani`, `id_ayah`, `id_ibu`, `email`, `password`, `api_token`, `nama`, `pasangan`, `tanggal_lahir`, `gender`, `alamat`, `telepon`, `foto`, `generasi`) VALUES
(1, 1, NULL, NULL, 'sadinah@mail.com', '$2y$10$o9NWyBrdkhBQWJFeWidYXeMHc5fxIAhyA1a3F2VAhmQ.9OolOyOai', '$2y$10$LmjCdz3QWp.nta0yB1mkL.xY91XGfbqhELmEPA55t0VIH8v.89Jr6', 'Sadinah', 'Buk Sadinah', '1945-06-01', 'L', 'Kertagena Laok', '085233889578', '1559961914.jpg', 0),
(2, 1, 1, NULL, 'magina@mail.com', '$2y$10$moJDvNoOllueIKMZNo.CreDyb0ysDK8vNXW12I0suyi04qNk2FRAC', '$2y$10$anMfC2uPw0nkjmkvgK26hOWjwPeBAVM2r/WUGVWOTesRnRkbW9drS', 'Magina', 'Condek', '1945-06-07', 'P', 'Kertagena Laok', '085233889578', '1559961733.png', 1),
(3, 1, 1, NULL, 'siha@mail.com', '$2y$10$WqVs0ouLd3y1w4YkFRBmzOHK6K9UtvJsXNPZtoVVSkmvpecLGzZqC', '$2y$10$w7FRNdV92NKlLBltPOmUe.lmOqkOapn77F7uSK3mPWY.YT62FwHeW', 'Siha', 'B.siha', '1945-06-14', 'L', 'Kertagena laok', '085236889578', '1559999438.jpg', 1),
(4, 1, 1, NULL, 'dulani@mail.com', '$2y$10$GQ5TSuVJBRpyad633DgOAe41RKrZ/OiOmrg5LACft7esXb0jjUKZC', '$2y$10$KffO8PJ94yzbR/k3BHodzOeAB33s6atYSj2qlJYt5jhjVOpNHa0Ga', 'Dulani', 'Misti', '1939-06-08', 'L', 'Kertagena laok', '085233889578', '1560000111.jpg', 1),
(5, 1, 1, NULL, 'syaria@mail.com', '$2y$10$lAlnoz14tc7eu4nUk4kCq.yR22qyN1ActhjbgDB7RA2DJkHCj9aI2', '$2y$10$L2uEP0u3UUGiIsuHg/58o.SFUCPIdO7QZP46h/ij8x12Iz4GiK1tq', 'Syari\'a', 'Sa\'ad', '1934-06-08', 'P', 'Kertagena laok', '085233889578', '1560000385.jpg', 1),
(6, 1, 1, NULL, 'sahati@mail.com', '$2y$10$JO0FMI4Trqzqnl5ziZBvLucyJAEttgem//0gSR24/cnQPwiiQwqBC', '$2y$10$n7Ti/R10b2tl8nh6h6.UKOnRDtrqrDWFanyub7VpBIKOull8F1qIq', 'Sahati', 'Munadi', '1696-06-08', 'P', 'Kertagena laok', '005233889578', '1560000460.jpg', 1),
(7, 1, 1, NULL, 'sahani@mail.com', '$2y$10$C3Ao5jvADe7Aw9ZFCP4EX.9yGAUPu2OJRE2PtzOuN5ovlxaXQgMoS', '$2y$10$6GoC8VLvm5Zw5WGuW8Wbt.zBm.H6mse8KbYi3tCwmh85GWst8nn2e', 'Sahani', 'Asmar', '1601-06-08', 'P', 'Kertagena laok', '085233889578', '1560000601.jpg', 1),
(10, 1, NULL, 2, 'Riyam@gmail.com', '$2y$10$8FNyfzkJLl58c3vL7mOLUeieAAI4OTxbTnGSuqbRmsDqQnf3NHAPa', '$2y$10$w5UXnfUwNTQPwrxEW1gBqOODDNNahR/niLftKh.RyUOdRtM.NlRmC', 'Riyam', 'Supatta', NULL, 'P', 'Kertagena Laok', '085233889578', NULL, 2),
(11, 1, NULL, 2, 'Atnoto@gmail.com', '$2y$10$dYYKgCXptJJyWbDUoVBoGes8WWAPa4Is69u2wHNVzoMHFYJV8GC3y', '$2y$10$gS.1klgFGhPv5XEYoGxWQu2xG/TPgI0KqzUMTxaB2cA2gd.coJ1Se', 'Atnoto / H. Taufiqurrahman', 'Bhunami', NULL, 'L', 'Kaduara Timur', '085233889578', NULL, 2),
(12, 1, NULL, 2, 'Abdullah@gmail.com', '$2y$10$pxAkP/ZRKi7EYPMZvJu/few4chUxXDW/FVXOcoz7SscAn19Aqrnti', '$2y$10$SXL.2rMgHXuGSr3rjnwKr.LgOj8IDFmpdbNy0kqSWpdsJ93iTIAoW', 'Abdullah / H.Fathurrahman', 'Royani', NULL, 'L', 'Kertagena Laok', NULL, NULL, 2),
(13, 1, NULL, 2, 'Parima@gmail.com', '$2y$10$vLpO76BEnaUKrfMlJybGIedQxjYUE2qYX01mkCigWW7Y7nOOMT2Sa', '$2y$10$JmMBzqc/EmpPMGPRYGC/POGZXgMZA.K6QPKIiHebV5.CwEzLjh6cK', 'Parima', 'Satrah', NULL, 'P', NULL, NULL, NULL, 2),
(14, 1, NULL, 2, 'AbdurrrahRahman@gmail.com', '$2y$10$p8zmoijxFl1x18Lkz8sfGOVy/BLMHQ7OvBnzFs8lSffAtUL7tANwu', '$2y$10$fK0k1oSZvLIgyavK5h5CT.voaBOvLc66BUslqT1j4m.rNpuO/yU2O', 'Abdurrrah Rahman', 'Fatimutus Zahrah', NULL, 'L', 'Kaduara Timur', NULL, NULL, 2),
(15, 1, NULL, 10, 'Sulipah@gmail.com', '$2y$10$NftgD5AUN9ccqLkdT6OQpebbdHyvDArsh8BRjPpaH59bk4rI3tAFG', '$2y$10$E1FxlHnj8v9xvpZSp9QVCujGpF0LBIwvct4ZQOgAUoqOh4Vzn02oW', 'Sulipah', 'Sahrawi', NULL, 'P', 'Sukalelah', NULL, NULL, 3),
(16, 1, NULL, 10, 'Zuhriyah@gmail.com', '$2y$10$5zuZnCeZiVUyhac3M5R7cuQJYBlX4CLnbXpsBsAdZhrFvE6t6oq8K', '$2y$10$BXtwo/gRXpRviMD7/oYUXesknI29fbz8GeVoYf3r6IibfIIJ.X4rG', 'Zuhriyah', 'Hamdani Ra\'is', NULL, 'P', NULL, NULL, NULL, 3),
(17, 1, NULL, 10, 'Rohna@gmail.com', '$2y$10$Vq.I5HYUWS/ZzOH11P51Re8Z3RntD.W3zEYil3GBBGig20A4beGrG', '$2y$10$XFjjO6s/6g8RBas0jL0iMe0ogw9QbwmSWYh4Jb1NKg4FhhWOrbeYK', 'Rohna', NULL, NULL, 'P', NULL, NULL, NULL, 3),
(18, 1, 11, NULL, 'AhmadTaufiqHidayatullah@gmail.com', '$2y$10$0231z7FOxnZVZlohZkfSE.3OVv9ULnAU7YBPhIm96Hmo2Qon4XYo.', '$2y$10$Rg8BYiDqxAaVHoXjLwc2O.xd5kMKcss80vjMqsm2Vp6arVA1qShU.', 'Ahmad Taufiq Hidayatullah', 'Susnaini', NULL, 'L', NULL, NULL, NULL, 3),
(19, 1, 11, NULL, 'Istiharoh@gmail.com', '$2y$10$lvFxgs945hJ8tNVbB3GhJeNQKUDlfxehZp.REOIXUWGYVNS4pfDJq', '$2y$10$W7GRvyeVVyX6GQ4nmnL2keI21p.X1N63rNTpvhn1Q/7iSFHs/DqZG', 'Istiharoh', 'Sunarto', NULL, 'P', NULL, NULL, NULL, 3),
(20, 1, 11, NULL, 'MuhammadBurhanuddinShaleh@gmail.com', '$2y$10$WAFbYUK2XcAjmF2tlXmZB.qe31QJPDfFNatzeTSw5GroPW2pwSalS', '$2y$10$L7eA5fASFYmSlrrgWkpctesC7ouMcK3yvh9QHcYgmPbbd23t.ASe.', 'Muhammad Burhanuddin Shaleh', 'Mar\'atus Shalihah', NULL, 'L', NULL, NULL, NULL, 3),
(21, 1, 11, NULL, 'NailulKaramah@gmail.com', '$2y$10$uJJewV5e.1hJVNJCXj1a5OsOwm4lcqyeeMWd/r4D6yWlNpsKyabze', '$2y$10$Uo3d1eMATqmYJO3/L12x2eo1wmbMsIslUz2ujkXExUJKEF4r1/vG6', 'Nailul Karamah', 'Ali Badri', NULL, 'P', NULL, NULL, NULL, 3),
(22, 1, 12, NULL, 'LutfiyatulFitriyah@gmail.com', '$2y$10$xWhTd78YErVJCfnLvYWTfOP0iX1o1NUgt9kvhitmW92RzehDZeVfi', '$2y$10$jfpcOBreVQK5nMnKv1B5QON7jccmIc5.kkIAZQNAiRff6qbRV8wWm', 'Lutfiyatul Fitriyah', 'Abdul Ghafur', NULL, 'P', NULL, NULL, NULL, 3),
(23, 1, 12, NULL, 'IlhamKhairi@gmail.com', '$2y$10$s65afcSUbpDlq5cQLiOkROfBswiEf4ubBszXj3BYl3A6T6hyEW1ba', '$2y$10$RGXpXNUFoMaar6TST9fDKuq3VIjNYDKizoVXJLiitBuxOzvkARPE6', 'Ilham Khairi', 'Cahya', NULL, 'L', NULL, NULL, NULL, 3),
(24, 1, 12, NULL, 'SyamsulArifin@gmail.com', '$2y$10$LXIHIrxeRIp18ztmtMRQ/.RN0ctJBjqtPJWBUPfthuKB9Trh/EZQq', '$2y$10$Tm6haCPxnVtC18wWFQ4.KORUHX6tTtw73lgoqXpyvM4XfVIqWGp3e', 'Syamsul Arifin', 'Isna', NULL, 'L', NULL, NULL, NULL, 3),
(25, 1, NULL, 13, 'SyaifulHuda@gmail.com', '$2y$10$J72XFwAF4HuBHuzkpC58Eu.vpKoBZuylBbOUddTSRaVIlqPEGOq6C', '$2y$10$Xi9XGogmLIgG2//4BJBun.Gkcced8X79c/9kQ.Td9BmlTc/6VDh0G', 'Syaiful Huda', NULL, NULL, 'L', NULL, NULL, NULL, 3),
(26, 1, NULL, 13, 'HasanBasri@gmail.com', '$2y$10$FT/jF2PUItZsxlDlp1TnPu2oZKnM1N/Y7urR/BF0jekkXE7GmEGAi', '$2y$10$xl0xNXzE07gSCAi/nfDQWuk/u8YqfMrm172HxUDp9wUNIBLowFaze', 'Hasan Basri', 'Rika', NULL, 'L', NULL, NULL, NULL, 3),
(27, 1, NULL, 13, 'AdnanHidayatullah@gmail.com', '$2y$10$nESYK3bZfx9Z.Aw9q5pGa.ow.9GvMopJoh52ZsB8FtvI6V1nbr1ea', '$2y$10$xxewaey4rHN7HxJ3KVgxW.Abv05tGQXPv68pNtuyAErfno4NjQlGy', 'Adnan Hidayatullah', 'Fina Rahmaniyatus Zahrah', NULL, 'L', NULL, NULL, NULL, 3),
(28, 1, 14, NULL, 'FinaRahmaniyatusZahrah@gmail.com', '$2y$10$4.CUTfBeCiYn/ahPJE3NiuhI4xguP/wt4I5AQW0xJhAfBWRSL5PYm', '$2y$10$S5NuYDPBytAFfKlbpxFIsecbwqczboWtFpz1dK3qFSx2xC0FObAum', 'Fina Rahmaniyatus Zahrah', 'Adnan Hidayatullah', NULL, 'P', NULL, NULL, NULL, 3),
(29, 1, 14, NULL, 'nurkholispragaan@gmail.com', '$2y$10$O478sNoMmvQnAq3K7Tm4DeEhlgeqqkpkndDcTgOP5zQpkTYtnkTha', '$2y$10$C5uX9v/mMZ7JMjG3BzGht.cayJqGpzO8qFZUIqSS7.YVCC5VVmATG', 'Nur Kholis', NULL, NULL, 'L', 'desa kaduara timur', '085233889578', NULL, 3),
(30, 1, NULL, 15, 'Ulfariyah@gmail.com', '$2y$10$2UtAyiJ6h.SDL/qqK8B8R.Rso5SiGVzerkrdWGkfIIOW03hf2AmMm', '$2y$10$1OSjmXtVgHW3MJO3uY4VZuYlijh6Xx6HhMEBAKPz2k0jFBT8jpAka', 'Ulfariyah', 'Hamdani Ra\'is', NULL, 'P', NULL, NULL, NULL, 4),
(31, 1, NULL, 15, 'Shalehuddin@gmail.com', '$2y$10$egq9l6DDE5/uTPx.s.jXjuztsP4DOG4SLZ1y8twJbD6SHdyREijh.', '$2y$10$aiIMrUB2lPY.otWNAobiluDwusYCJyqiasy1eLXxHafpWwPmnvmxi', 'Shalehuddin', 'Tatik Sulastri', NULL, 'L', NULL, NULL, NULL, 4),
(32, 1, NULL, 15, 'ZulfitaSari@gmail.com', '$2y$10$EFG1ADyvAG/PzoJ2iCoP.OQMRoqMgYCS3uq9Qtn5AUQ1Y68fkyfuq', '$2y$10$j.2JAoxDTDCWCRVtW/UusOiRGw/L.7EXa2LXl4Jh01wiCWMI0pWja', 'Zulfita Sari', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(33, 1, NULL, 16, 'BaiturRomlah@gmail.com', '$2y$10$6lpybbkI7hslthz309mRDevjxjsfCxCL2P6jv3Z40Jyx.OSGZvgUS', '$2y$10$7BDQtcIePBkltsTgOpY.qe3G1lhn74II87.j6mn5sxCWPa1C8xr9y', 'Baitur Romlah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(34, 1, NULL, 16, 'InayatusZuhriyah@gmail.com', '$2y$10$YS0SMavFLtRkZxIrXB0q..KZTDdZ9Oyhwb2KaMigoGewSa/u21Q46', '$2y$10$u9IrwLlm7Goyy7.uB1l0hOOSrH53JBu4L6m8lHwy6SBhbnArBFbo.', 'Inayatus Zuhriyah', 'Suttrisno', NULL, 'P', NULL, NULL, NULL, 4),
(35, 1, NULL, 16, 'MillatulAliyah@gmail.com', '$2y$10$z15xGgbeK7kxaeNU/cJkLumthLCZabRCga.E4a4S346lRb0WROU5u', '$2y$10$CNNbDAoJCj8O2F6yljRaQele7.1g9cwYUF5w2Kr60NREfdfJUhLMi', 'Millatul Aliyah', 'Abdul Waris', NULL, 'P', NULL, NULL, NULL, 4),
(36, 1, 18, NULL, 'ChairulUmam@gmail.com', '$2y$10$woZ3/hV9lDYjvcV1ERK1WuMmcjY6UzQHA9.noo/ZUfbr8i/G6Po2K', '$2y$10$YACm5K1pRFhAGEpISEc3j.DBQfNWb1SqYxanHz7.KnUXySEmQgjAu', 'Chairul Umam', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(37, 1, 18, NULL, 'AmeliaFirdausilMa\'rifah@gmail.com', '$2y$10$/T/Br9LmPa.VzfN5KCWl9O1Fm6bUOWN1Ra9jBP1R8XTeS40o.bjPC', '$2y$10$Ftwez8xhlTLaGkYuv3WA1O4qnYmqTpGcohOE5kXUPaAsMye1ZZg52', 'Amelia Firdausil Ma\'rifah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(38, 1, NULL, 19, 'ElfiyatulFitriyah@gmail.com', '$2y$10$0Lc3mO6doHKnEgRBIRkd3eS.lv5N3y/eMCjrL521DYDq8NptNRU02', '$2y$10$M5FVw12p4NRBUC.YvjqQme2UK1V44XICSi5ami6P2AjSLM6E2fCn2', 'Elfiyatul Fitriyah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(39, 1, NULL, 19, 'MiliVirasatulMufidah@gmail.com', '$2y$10$Arukd67DZ/ZhqnQageQY.OfMpPlEv7BGfnZCGoxFfsRC2GMO8F9Um', '$2y$10$1E5agkLpCRqRQth.mlCK..Sg6SeUz/3AzFkwW/515bTLtmUlvLWHm', 'Mili Virasatul Mufidah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(40, 1, NULL, 19, 'AhmadTaufidTajulMuharram@gmail.com', '$2y$10$IHsfZD09lXuFBlT3BE8lU.i2MR6KhFinQaiiDhSvMZ0SVooVrtyte', '$2y$10$wJ2U9aiRDIGZ7/FGyWRuWuZybd8vopm4hpIYUb93ldo.jooyXRaJq', 'Ahmad Taufid Tajul Muharram', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(41, 1, 20, NULL, 'ZamidFaiqAbdullah@gmail.com', '$2y$10$shE7N55k9gpA5Aq1yz7OtO.ZNhBaPxE/aM1hQ0lO.ljnTHOJiHdYG', '$2y$10$1NADHI8tsyczs1ZeI.eDs.Nln1Qwu2HnRCTgbQs2cUTU/TiX7lOaG', 'Zamid Faiq Abdullah', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(42, 1, 20, NULL, 'RosaAqilatuzZuhlula@gmail.com', '$2y$10$VcHsR41mlKd5wvQaXeEYpe2t/N4bbwZZ5oIzn7wRRDnR33FMqV7y6', '$2y$10$hHkfXqrmIHqFtBvyebjcju8iElDjodelEwz8RCsM.3dpThOEWc6VG', 'Rosa Aqilatuz Zuhlula', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(43, 1, NULL, 21, 'MuhammadFahrurRaozi@gmail.com', '$2y$10$Us1qwFJbT3s4LKFrhajPhuuKv80gLtP.rJuAnmNyo7segqy/EEVhO', '$2y$10$xK4Etp1.T80QIGss2VtdTeu0GG5yBoXd/5MFA9ukMWsEVlGQZRgMi', 'Muhammad Fahrur Rozi', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(44, 1, NULL, 22, 'MuhammadRizalKurniawan@gmail.com', '$2y$10$YmCWQjiPE2ohr0Gf.BfAvOdbJ13id3bL.AQqmopxRhy0NowLoucV6', '$2y$10$k5IjIi8CadFdA9Xjk0m3WOl7fQDvg7Gz8HzwfMoEm7074saz/DdXq', 'Muhammad Rizal Kurniawan', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(45, 1, 23, NULL, 'FirdaJauhiratulMaknunahKhairiAbdullah@gmail.com', '$2y$10$7HDOrptNzjfAiiG/rnS1w.I0tPh7UZVmbCGGXTll5QhB9v90AtHzy', '$2y$10$fy8HAsDprkf1RyyFPhhsqO/BR6A6BPq0o6RSefBg5MgX5xkUc3WmO', 'Firda Jauhiratul Maknunah Khairi Abdullah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(46, 1, 23, NULL, 'Aufar@gmail.com', '$2y$10$/.NG1BU5cDqR8ZSSyY.YPuL6e2wkc6Mb8ZvOTHIrb8C1AGo3E4Di2', '$2y$10$3FFkVc8j/88CF2P7QaxJYe0sJbBcfJOGESV.uR1GlSketAqHlgjsW', 'Aufar', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(47, 1, 24, NULL, 'MuhammadThariqArifin@gmail.com', '$2y$10$lnZSqaODgzccxpSl0nseg.e99w4UrCjBXfP/sl8einI5f97kbpGOK', '$2y$10$EebcQz4VpYpM8eapun5hcON2U7lc92S0oq3HGK1TGYicqmuOPODTm', 'Muhammad Thariq Arifin', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(48, 1, 26, NULL, 'MuhammadEkaArifHusna@gmail.com', '$2y$10$N4TG7bpWEWtyuathzP1JfurpSq4l/UoNWA/NX7CHgAeovkHQc1lZa', '$2y$10$5zTdkbnUMY0nlLlBTPU/YOwavXZVKLgEF2epG4Ayg8vbHYC2ZjQ1y', 'Muhammad Eka Arif Husna', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(49, 1, 26, NULL, 'Undefined@gmail.com', '$2y$10$/mPE3rpIap80k5F6y4F/SOZ8XIB2NYCb.TraES7EjEeQIuuhBO3X2', '$2y$10$rsgtQjTEtTAYl0TQu1VsHeOU1NydKCznrTR6S386stLGTlEAzb6WW', 'Undefined', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(50, 1, 27, 28, 'AiraSyakiraRahdatulAisy@gmail.com', '$2y$10$c6EDO5ie46w6W/rupbvR/OPA9WIYOSXzpVpCzB0/amcFPll5x26k2', '$2y$10$2.NlaPRQoIgLLLK3NZPcPOhVpRlvdZ5d/H6QDwhVDF9QVU82LL22G', 'Aira Syakira Rahdatul Aisy', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(51, 1, 27, 28, 'NasyaPutriAdnani@gmail.com', '$2y$10$k/pZkpHkehzIWdEbme28xu5TVYI7agYlRvy5.H88qVWxgrn7NvYSa', '$2y$10$M.aTDYf8tSaw8UB9CK.Y/O3q.70fbDBBhNqZu4q2CziQc0S5.TbLa', 'Nasya Putri Adnani', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(52, 1, 27, 28, 'Syafa@gmail.com', '$2y$10$ERNGtFGIXGU736m/KgNqQ.6es1gWpwJeIFVfc4AICcTRGcUWKitui', '$2y$10$3sX5/axRkTBRQ05usYa/zeDBTxji2dIidgkjFmTI36bpdYMYSUK6e', 'Syafa', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(53, 1, NULL, 30, 'Faqihuddin@gmail.com', '$2y$10$emIA5aiP5Bi0Z4jsBJTO8eWNCt/MZyjyFJCcGgx4tfk0LeUZZQifW', '$2y$10$P1JA7rEOmG8cGDIQj8m1Z.V2umJuY4be4iPSlezhRI5zJu.ERfTx6', 'Faqihuddin', NULL, NULL, 'L', NULL, NULL, NULL, 5),
(54, 1, NULL, 30, 'EstiHalimahLizamatulKhairina@gmail.com', '$2y$10$sz1m3U.1J53uHsU8SFwFSuljAU0eytCXrSFG25f.mtFk/DeH1SYGG', '$2y$10$PhrQ8/RRAwP0A9s7g7luE.9LKrhscHM6E6eKZ6nQl5H3y./rBIfsy', 'Esti Halimah Lizamatul Khairina', NULL, NULL, 'P', NULL, NULL, NULL, 5),
(55, 1, 31, NULL, 'MuhammadNaufalShales@gmail.com', '$2y$10$UmxHqT0nPeYKpy/jb2i.DOhj0h8mD9dL0VBXrGbsaT7MUyy6qq3Ne', '$2y$10$EmKGfBT/NlD2Jo0fyVEZOOKwVMjB/N8Ozb.X/MgLGMt5YTs/4hyam', 'Muhammad Naufal Shales', NULL, NULL, 'L', NULL, NULL, NULL, 5),
(56, 1, 31, NULL, 'TashaNabilaPutri@gmail.com', '$2y$10$OuUg4WnfDQ2bqGdVbZihBeqjt01slDlT6zV3ks21UlTBpjGQhfxfK', '$2y$10$j.mqsKZto9kBmEJOYHUMNe2.5t2zj0PRwkTCHNDgFNfzr.5uXlDdu', 'Tasha Nabila Putri', NULL, NULL, 'P', NULL, NULL, NULL, 5),
(57, 1, NULL, 34, 'KhairiyatulMuallifa@gmail.com', '$2y$10$lo3j9TrnjXNAXgDZtp72TeZzsv5ntTtCQEYgxJIMLjr3RrPoZC8kS', '$2y$10$Y2PO/IOTs2fmr39ZV3mVY.hOTRWHYFnWnspaXAtS3ypKwrZdePhAq', 'Khairiyatul Muallifa', NULL, NULL, 'P', NULL, NULL, NULL, 5),
(58, 1, NULL, 35, 'RafikaJauhiratulMaknunah@gmail.com', '$2y$10$9Ila1hubqXuZMIxbKb1juOFJWHxFnSAdXyKCSWYWnH6E2q7VYL1o.', '$2y$10$C4fNsm11FO288G6GAb2nle3sw3vH5xwxW.SFuSoDsrw/BNM7l3Kcq', 'Rafika Jauhiratul Maknunah', NULL, NULL, 'P', NULL, NULL, NULL, 5),
(59, 1, NULL, 35, 'KafiliaHilyatulAzkiyah@gmail.com', '$2y$10$QjE9VCCxlWg9nEURQm07YuQYgNL9VGPsbj/pLCYm0hlv9F5Eu7M8K', '$2y$10$AD3dPv8nzY33lDrVFlYhnOxdbySWnAKP.K.slPpszHWa6BA5kFWw6', 'Kafilia Hilyatul Azkiyah', NULL, NULL, 'P', NULL, NULL, NULL, 5),
(60, 1, NULL, 35, 'NaufellinNahdaAlfatunNisa@gmail.com', '$2y$10$NveE.3vABVLhM118aEgx4ezSiDeNqAk16cDG/B2xocW.1/rKBJKbG', '$2y$10$22//OPFCGBztT5A8CPUdPuHan7SGPiVYRGmZNx.N7TG7tO9eDUkpO', 'Naufellin Nahda Alfatun Nisa', NULL, NULL, 'P', NULL, NULL, NULL, 5),
(61, 1, NULL, 6, 'Abdullah@gmail.com', '$2y$10$xRI1RzjHtD.PIzPmpMlhhurjs7042.Fmr9J40XjYEgvXXmPc6zGLy', '$2y$10$Ch7qVR.9XxFqVDwjkvcCve9bQJSArxH5IR0va3em4odNa9.7XwWkO', 'Abdullah', 'Sukaryatik', NULL, 'L', NULL, NULL, NULL, 2),
(62, 1, NULL, 6, 'Muslimah@gmail.com', '$2y$10$NsizuEv7oeUyUzJCyIcLTehXR0PVPc34MrsfRwBcpWjw/BWTV6Jz6', '$2y$10$lTKax2Dz.xWmKx/SK.QHxeGriFNNkwZ/6aEuukkGaQLDV9x4hooA2', 'Muslimah', 'Muhammad Siri', NULL, 'P', NULL, NULL, NULL, 2),
(63, 1, 61, NULL, 'RaniaAnindiaAbdilla@gmail.com', '$2y$10$IYlkgpe7yiph0lpfMapanOhUZTXQZrvqSqg1bYuJZX056z11H8ELy', '$2y$10$Qkg357dDThNVU6DbhfQKAOJ8oYPY8nckcKtaTcK34BkvP/.BfrcWu', 'Rania Anindia Abdilla', NULL, NULL, 'P', NULL, NULL, NULL, 3),
(64, 1, 61, NULL, 'RikaAuliaAbdillah@gmail.com', '$2y$10$rpNEYuMg34ONOheqUXfOiuPPKNqdj5ybjb/7uruZWl8tR2sCuA.zq', '$2y$10$sATMf2fCLO/eh9c.oOznHOMG96wxOvdAxDGV7gk2Dj1KxFZAm1xlG', 'Rika Aulia Abdillah', NULL, NULL, 'P', NULL, NULL, NULL, 3),
(65, 1, NULL, 62, 'maftuhah@gmail.com', '$2y$10$a5czGkobdva.dgIcshT86eUFL0Jwn/wNY1Y1zKk9.CIXVPMrIKVTS', '$2y$10$UXrds1wCb.CFsyuLDxuAHeaRLxXg4ic7RjbOv6uL8vM3j6RY6pbFq', 'maftuhah', 'Abdurrahman', NULL, 'P', NULL, NULL, NULL, 3),
(66, 1, NULL, 62, 'AinulFajri@gmail.com', '$2y$10$JLprdLZ7qunjpxhn39v8cey1TgG/DFH666NHrO/XIATqV1almiO0m', '$2y$10$cxDJ68JG2.GGjVIX7gDkF.v18pxIimq9wx4ezZDxrlGzCKMvUmj1q', 'Ainul Fajri', NULL, NULL, 'L', NULL, NULL, NULL, 3),
(67, 1, NULL, 62, 'UlfamiAisyah@gmail.com', '$2y$10$4me5HNBnQMvntZtH1OVHeORVEKjEj1PiJEKJKR9RwrH4dtTpNvjQC', '$2y$10$wMdlj2D0WiP/VXOKQyuF0evT1OarqT29T97fVAYtXLv2VAYwVp1Qy', 'Ulfami Aisyah', NULL, NULL, 'P', NULL, NULL, NULL, 3),
(68, 1, NULL, 7, 'Holila@gmail.com', '$2y$10$R8lEXZO.JQfRatR6zlocrOxyfOziYBS5RJMFXiVV60vX/04KVm7gW', '$2y$10$omNQqiUnnU/MmoJMltawpe5dHM9/1yVASGqsqO3hheA3npzKGVNAe', 'Holila', 'Ismaniyah', NULL, 'L', NULL, NULL, NULL, 2),
(69, 1, NULL, 7, 'Kasyati@gmail.com', '$2y$10$gcLl0BENiYua1TX78iXFLew/PodnRWlR4WfoNqzjD31FIZyFOzSju', '$2y$10$9QbDzDGayOGTrYLjGUivbuGZ8h9Ll54nP8xybLS04t7FQ6fqOAb/G', 'Kasyati', 'Muhammad Dahri', NULL, 'P', NULL, NULL, NULL, 2),
(70, 1, NULL, 7, 'Kasyana@gmail.com', '$2y$10$w9gw412ILSH09a5ELVbxUejWSeNwUkxun1QYFWmJWFIJajCy8MEbK', '$2y$10$3Dy.e0cL.F8YJ.2s4sjf1utojcSQK.R8Wr09.O5hL87KNwXDF5A0.', 'Kasyana', 'Suhdi', NULL, 'P', NULL, NULL, NULL, 2),
(71, 1, 68, NULL, 'SitiKamilia@gmail.com', '$2y$10$Qf92EsXh3HdXxdPiibfixOUAF8M3VhvyEdY134HfQw4Ut6WZSEknK', '$2y$10$omaSngynEC4MbtqnRsFGGuJMT/LjSpvQwV87U0K3IAYMVXStIptJW', 'Siti Kamilia', NULL, NULL, 'P', NULL, NULL, NULL, 3),
(72, 1, 68, NULL, 'NailulQiram@gmail.com', '$2y$10$JqJjGYDAlGbutdQegiJNgeCS5htTrtsa1cn.JQEt1EM3Abh2MgY2.', '$2y$10$5PxEgBU7zZ8yYlVdEFTebOU/UKb6TU3P1zNhfN4RJISwu7Uj1vgKC', 'Nailul Qiram', NULL, NULL, 'P', NULL, NULL, NULL, 3),
(73, 1, NULL, 69, 'KarimatulJannah@gmail.com', '$2y$10$OYKklvxAkZDfHWN2.NHG4OSD19Y6LpZ4O6g.Vf0ae7zncj.aEPQsW', '$2y$10$lhcWaUaYi1.M8uVzWn.1A.K7zlLoN7J2NPlmbdZIxhrrViVcXiaPG', 'Karimatul Jannah', 'Hudairi', NULL, 'P', NULL, NULL, NULL, 3),
(74, 1, NULL, 69, 'RofiqatulAiniyah@gmail.com', '$2y$10$sbDMuJsdIn8h6jHuT5AGSe6K5G4gphguBGmT1n2g2b2jhwyamuV3m', '$2y$10$R.lkrV5X7O/EjQ6NIWni3OXxwmog9r8HUQrTj5XuK8q8rqhNMMpd.', 'Rofiqatul Ainiyah', NULL, NULL, 'P', NULL, NULL, NULL, 3),
(75, 1, NULL, 70, 'FajarRidwanIrwanto@gmail.com', '$2y$10$2Ks037.kVYxbx5RqdjASyu/xEut2cqeoS0ccJj3k9rbjSI8fbrclm', '$2y$10$0wqjEcWUGk0FGK8BvIAOO.sD1wZHayzaAGxO07Cht9fDVfeWJKB/a', 'Fajar Ridwan Irwanto', NULL, NULL, 'L', NULL, NULL, NULL, 3),
(76, 1, NULL, 70, 'DinaAidaFitriyah@gmail.com', '$2y$10$2V2tsfdZjt5YOdNyY77HT.XrNh.sM.AMrJh4WT.uHH0RZZQ6Ztu5G', '$2y$10$VFQewfX9hbKS1nV5RnuWG.PEojJPAxm6b9/0q6bl2Wxq1CFFm.mhS', 'Dina Aida Fitriyah', NULL, NULL, 'P', NULL, NULL, NULL, 3),
(77, 1, 3, NULL, 'Asyani@gmail.com', '$2y$10$NmI/r7X0dHfvy.sPmZVPQe6Rot40y/C5wNyaEWNXW96fECfuM.Ufq', '$2y$10$pihw.p971IThUKvwhScvQOl9jJSSGSd67GWEnCzL0qDW605mNqKZO', 'Asyani/ Hj.Fatimah', 'Buhral/H.Hasim', NULL, 'P', NULL, NULL, NULL, 2),
(78, 1, 3, NULL, 'Ja\'i@gmail.com', '$2y$10$Mr/6qw55uLM4rh19GXgKPu7KRlnUTTlrHMLqJ.lv5xwnd2SrvcSmi', '$2y$10$2nqNT8XBFKxB.MTh3ArJXejFMq2qDjyKzqz6S4g1r04N5nC78kCt.', 'Ja\'i', 'Suti\'ah', NULL, 'L', NULL, NULL, NULL, 2),
(79, 1, NULL, 77, 'Rupyani@gmail.com', '$2y$10$mF/MxYUkPcNantlWCzrjS.RMAyoJopP.sRBVMQnOUk9g31FDpZ7Z.', '$2y$10$XgerPS5gl25Sr0MSMMtBYOPkXM96QLYcytlTF91Ulq1sIif1BXeCG', 'Rupyani', 'Munawi', NULL, 'P', NULL, NULL, NULL, 3),
(80, 1, NULL, 77, 'Jandrini@gmail.com', '$2y$10$8mthuJ6MurFbncA4ES1Fz.RxbQ1LXEbm8ljuWe08Sgc8zQG0K516i', '$2y$10$gxQUJU8x5etK6YbRmpYTBOMVe7RTaHD6pNK93Xmxvt8QbknPnreBK', 'Jandrini', 'Muhammad Thohuri', NULL, 'P', NULL, NULL, NULL, 3),
(81, 1, 78, NULL, 'Aminulla@gmail.com', '$2y$10$Z3vN/78WYVBk7aTzFO98KecoYhUClyMAD0zr5HsqPr9zZwy5S.z.y', '$2y$10$iiGINlmrL8FvZL4dg6B8WeOWGQI1xRV8BENo9JPMrmi6pigfu3lFq', 'Aminulla', 'Evi Fatimah', NULL, 'L', NULL, NULL, NULL, 3),
(82, 1, 78, NULL, 'UmmiKholisah@gmail.com', '$2y$10$KWFr69ob68Fg4DylW2B0NOQ92HDMXbN90qZGSikGcQ5XZYQOTiYlC', '$2y$10$Boe3w.tPsEiT6mH/BokCWuI2ZUma5Eht4Bfch6QNQqeKRvuGURSIS', 'Ummi Kholisah', 'Muslimah Khairi', NULL, 'P', NULL, NULL, NULL, 3),
(83, 1, NULL, 79, 'HujjatulHasanah@gmail.com', '$2y$10$T7KPnEciK5C7/SGq7aFG8.rlnNRzU.WeWaVXM0Fp5kBG.x/0PQzwW', '$2y$10$.Zq2Ta.HWD5UU2rd0rWZx.mEAjzH278aQFZMdgJfAIoW834tqpGYq', 'Hujjatul Hasanah', 'Hermanto', NULL, 'P', NULL, NULL, NULL, 4),
(84, 1, NULL, 79, 'BadrusShalih@gmail.com', '$2y$10$bwU2BQPuzWhCp00UuILK/.fOZVJjf5kM9TeZ.Q4GqcVnlHIMLOVIO', '$2y$10$AFq6lDJ7TRZ6PPtc5b5Ase0JMZ3nWfBCLiot7dO1l2Jn7yd3qKkgi', 'Badrus Shalih', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(85, 1, NULL, 79, 'MalihatulMunwwarah@gmail.com', '$2y$10$MFRUMrP6oLbpGoEYS.2LC.ohmsJ7nQNz8ZckIthEV4QEvB7/xdM5u', '$2y$10$2T96AtjxuFtKlEtdSSz87OGFQ39dHr4yS7wH5Q/D0ea9/xL3cowJW', 'Malihatul Munwwarah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(86, 1, NULL, 80, 'QismatunNafi\'ah@gmail.com', '$2y$10$ZdVowxakG4M35EUzDgCNQuc58JKPxhdqRlLyomHevl/rb46n4KVFm', '$2y$10$KWlBZf89a6CZO9oz3Oe6MOvBloyAj.ynxfDGOeI/80Fe3DnFEIriW', 'Qismatun Nafi\'ah', 'Achmad Fauzi', NULL, 'P', NULL, NULL, NULL, 4),
(87, 1, NULL, 80, 'Mar\'atusSholihah@gmail.com', '$2y$10$8hvx4wV9Us.Jn28hCJlCIO0YBkoV/InBkghIzU9YMrtuJY7V0qmnC', '$2y$10$eNOyrB3EkBuvJD6znqaaYOvKYzFn26WSvGBy5Qss/JQ28w7gUQqXi', 'Mar\'atus Sholihah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(88, 1, 81, NULL, 'MufidaAmin@gmail.com', '$2y$10$PWG/s6k151bCeT2pVBlrauakS6aUeX2UtHbREuwSYDNd7enVbqlPm', '$2y$10$2kxPPalQLRDQW1LAMD8XLewm2KPhVP80wU7xuzhcK0SiO56kwdwe.', 'Mufida Amin', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(89, 1, 81, NULL, 'DelisaAminEl-ifah@gmail.com', '$2y$10$hJcEdPjWKBaM4Q0.8x95kO5Ig/tjciw2WBYSjls/FXV7OkKEwDlou', '$2y$10$DnRSEnefN1wBjNw7gta6Def3WdG9prlf4G4Lo/NrLCcpnPS89EO52', 'Delisa Amin El-ifah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(90, 1, NULL, 82, 'SilviaHanunJamilah@gmail.com', '$2y$10$O5oi4t4k/149oY.tKZCe7ekhmJSX9.de3YShmtV0UBnFdgVotrV2K', '$2y$10$GEaVkyEjBU.A7lzKLz3Qke5cr61U.WOuJyRJoSYnNzpEA5XQBim3m', 'Silvia Hanun Jamilah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(91, 1, 4, NULL, 'Munawi@gmail.com', '$2y$10$xSJFfzrOfaumHsREk7j1sup9jWxW3mj7tFD4bl6J1Djn4kYf.z9k2', '$2y$10$1Lgsjd1FrK.K0Jvae1i2eOWwbydiPFdpAzd7oZStSuBhhNkyp1WpW', 'Munawi', 'Rupyani', NULL, 'L', NULL, NULL, NULL, 2),
(92, 1, NULL, 83, 'NafirNafi\'ahZulfa@gmail.com', '$2y$10$BivNjRhhaiNaVg2DjlXc2uGR5eZ.O.62rRLZ0/uXbokOxNoGpZc2e', '$2y$10$E0f6fP2cGEOihP5ryw7XXODG0g29v3YcaEpApWkQcGMO4p5rXiqAO', 'Nafir Nafi\'ah Zulfa', NULL, NULL, 'P', NULL, NULL, NULL, 5),
(93, 1, NULL, 83, 'AchmadRizqanNafi\'@gmail.com', '$2y$10$uhdkaNB0ucqDNDkU7G0KwuqjR8nHAiHOMNxYBYfHLF7bsSrd89uLK', '$2y$10$6OIpWkjRfaFob2HbvrHxfOAJDkMOslKSvcWOo1JrKPusDwqGWnJQW', 'Achmad Rizqan Nafi\'', NULL, NULL, 'L', NULL, NULL, NULL, 5),
(94, 1, NULL, 5, 'Manidah@gmail.com', '$2y$10$61CfQDtRg/1YEHUPoGwDu.H9ft3yx6SnSFieRgdVgYuuQhCbjPciC', '$2y$10$FzjjOXCS9Zwy0NBVyfHvsewUCFjcTTcEoc/Zb016POvTH0QAx3kJ6', 'Manidah', 'Supandi', NULL, 'P', NULL, NULL, NULL, 2),
(95, 1, NULL, 5, 'Suttiana@gmail.com', '$2y$10$k/.9r/v4nSHSwFLZjMxjuO2j.ePRXkRnGmw//SBYKVhyLgNCxN/Wu', '$2y$10$omx8kh7mAn76QfDhmrgLeekpXQdO62FbixK76cETvQZGO1jD.qQQG', 'Suttiana', 'Dulla', NULL, 'P', NULL, NULL, NULL, 2),
(96, 1, NULL, 5, 'Sumar/Hj.Juhairiyah@gmail.com', '$2y$10$i38NtNNgtoAJVv7Kc3NYm.mI.NDk8vALxulNDOoyFZIQdkCs388cO', '$2y$10$c/yj1Pp/z4CCd40OimN4LuFdSNw0d99E/tFsB/e/DPGRhb6tRGqu6', 'Sumar/ Hj. Juhairiyah', 'Mad Duri', NULL, 'P', NULL, NULL, NULL, 2),
(97, 1, NULL, 94, 'UswatunHasanah@gmail.com', '$2y$10$AN2dv1xRNYTaV0dzw7kEteShNCiddgQvdRzdFveBfdK1sbyAplAMK', '$2y$10$6PHGOWJMZ870Cf5OL9k4kuYkWTvFhialaRs5x15rEwRxAEtlbsM4C', 'Uswatun Hasanah', 'Nanang Eko Setiawan', NULL, 'P', NULL, NULL, NULL, 3),
(98, 1, NULL, 94, 'UsmanFauzi@gmail.com', '$2y$10$7I/Sa44FmGMiFiq5/xOdqOelz4v7ncojH7nb8SBk0raSkl41f7pxi', '$2y$10$nYF1ski2ZrEOUKvdKFcIruKLLTKwzF01tVEs.84edv4zN43mGHCbe', 'Usman Fauzi', 'Luluk', NULL, 'L', NULL, NULL, NULL, 3),
(99, 1, NULL, 94, 'IsrafiyaMufida@gmail.com', '$2y$10$tMwkRSe6wpUeJXlZPKb0zegJ5QDmaxwfpI7TR5qgwVKH5RW7MuWM.', '$2y$10$RU7EqIJIo4t07xERwleRfOY44wmlQc67wtnuGSMsQdKnmLaByLLrC', 'Israfiya Mufida', 'Heri Siswanto', NULL, 'P', NULL, NULL, NULL, 3),
(100, 1, NULL, 95, 'SitiHasanah@gmail.com', '$2y$10$HHGq71Fphj3YP9nUP.LJHeDo2VWbiJLZLplJlx66AasdVThdtOFCi', '$2y$10$J29IQtmwK40dKDfdig28Lul/vnyyGhe9MOnde7u/41RVh5uJuOcSa', 'Siti Hasanah', 'Ariman', NULL, 'P', NULL, NULL, NULL, 3),
(101, 1, NULL, 96, 'Muflihun@gmail.com', '$2y$10$D.vGMikHnLJ0iUR0h2WPR.hzOCda6sqrn2TrVG24r2gJnuPUg7CPi', '$2y$10$yKrwdCxUKH8Z59hcldMgb.Os03QpmZEA9OAqm4gk72Eovs0rjbhUy', 'Muflihun', 'Mei Uningsih', NULL, 'L', NULL, NULL, NULL, 3),
(102, 1, NULL, 96, 'MiftahurRahman@gmail.com', '$2y$10$qjhsWd70LUwPb8mNWQu8Bet4MmesmcJKgxSf5Urfq3BA1A9OAUkXC', '$2y$10$x4kgWq9n5flDZunv6rQ4kOUIaJiWL9M4Pz5UdQbf7yk9UWS2CeubK', 'Miftahur Rahman', 'Puput Puspita Sari', NULL, 'L', NULL, NULL, NULL, 3),
(103, 1, NULL, 96, 'BadrulMunir@gmail.com', '$2y$10$qop605lc2JMxumpbikZouOdpRWkJDBYn1forGeXFOG9rCCt.vtrrS', '$2y$10$kLe7eUCvVKNhG2dJlSA0GO.Pj4j4Qd0qpyVjZHFOZ6MbcCAWE1iFm', 'Badrul Munir', NULL, NULL, 'L', NULL, NULL, NULL, 3),
(104, 1, NULL, 96, 'LailiyaturRahmah@gmail.com', '$2y$10$tdAGBqt2pyZjgdaY8GXxc.RB9.C.QIAYyUndhFWyJth8k3v56TZXO', '$2y$10$NDhwkCQdNVhKdfo/5gVWdON8XipUP0JETzsuoL37mnXfhhOeX9/3i', 'Lailiyatur Rahmah', NULL, NULL, 'P', NULL, NULL, NULL, 3),
(105, 1, NULL, 97, 'WalidGhaniKhairani@gmail.com', '$2y$10$DnkIyMSYm1GlA7juKK1.5OGfm1QECaY6bLeSIDl2b8HnugCF4fNmK', '$2y$10$t2lY3Z25KDJC5.COLUs/yujL2eiaRyyjx1foeYEH4ApBDCiSyQ7za', 'Walid Ghani Khairani', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(106, 1, NULL, 97, 'AbidAbidzarPranajaSetiawan@gmail.com', '$2y$10$6Zwh8dgQ/cQGffddSjkOn.y1HBPv9lKYh5bJOjJWtg5o4dnhIKVPy', '$2y$10$OZbvsQMHDTgWpcHqhPjznurqsGgJjD8nRmwt7yhsTTLwGQRlXSEhK', 'Abid Abidzar Pranaja Setiawan', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(107, 1, 98, NULL, 'RanggaSetyoNugroho@gmail.com', '$2y$10$TXL6wvv2qe4VcfmbVyNTXOPo14pMUg44T1c0kWQIKAn2i.wMTqKES', '$2y$10$IXiSDXkFFKUJ./04XefG0uFjUb4P.VbzneU16ly3o6JpXd/wZ9HPm', 'Rangga Setyo Nugroho', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(108, 1, 98, NULL, 'NurilFajriRhamadani@gmail.com', '$2y$10$Wca3CLGcc1ZJUzZ98pPuneWC4q8mUMGErgw4fl1FimdKQAdJCUeoG', '$2y$10$NXbuhx/aTY3qmyJJ5UJY7OZbrl3BgKTgpWk6cejni9pcUBZZW3LAm', 'Nuril Fajri Rhamadani', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(109, 1, NULL, 99, 'HafizWildanSofiansyah@gmail.com', '$2y$10$horGqzTKcrAjCvhbfEjgYetyAXmBS2.e5gJwY4z06uI1em.srxiei', '$2y$10$6X0qWZW189Sg06ltINCVQeXTD4XzyRxdb4H0Qtr3UEhW333uP284q', 'Hafiz Wildan Sofiansyah', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(110, 1, NULL, 99, 'HafizahIzzatiPutri@gmail.com', '$2y$10$4onM9Hl6p/erC0tGR3af9uKP9QJBC6wouzuXkClWDVQs5cnLIZ8JO', '$2y$10$0ubev2v5UUGoDzm8T11DAOttN9I9q0S/JPhk3bCjaVID9kx1kzcxG', 'Hafizah Izzati Putri', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(111, 1, NULL, 100, 'ArianaIsrafiyatulHasanah@gmail.com', '$2y$10$Dhsi1tYy5U7bXbha1n7af.GY.wcBNtt21MSASShm3yXVB/PT3gRjm', '$2y$10$QZpBFphuNdja/RjME7ChgODi3F32JYT9ywY77tLv2iyJ31k762Apq', 'Ariana Israfiyatul Hasanah', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(112, 1, NULL, 100, 'ArmanAhsanAbdilla@gmail.com', '$2y$10$LFaSdJeYSScfYconA89XR.i9xCqwa3zYJLmDoe52OyS3eC3wASqfO', '$2y$10$HSFgfTNdT39nCRuYKNEpheGpU8.vs0Rt4iM2K08eLMS6l.XcmZNiq', 'Arman Ahsan Abdilla', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(113, 1, NULL, 100, 'AmiraDerajatulUlya@gmail.com', '$2y$10$Zj9Ln/Irtdbv3tZULvd8h.273yoSoqJyqtmtjRHeydLN0uEH3.ig2', '$2y$10$56it6uv3X5XHp45osUZxOuHT2dWErgZdLzPMJ0vAVEDR1thKyd9sq', 'Amira Derajatul Ulya', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(114, 1, 101, NULL, 'IfanMaulanaSaputra@gmail.com', '$2y$10$E2bYRfRjrdD1JnKN9b1DoebUbvH1H3iMW/nJRktwFrnyHGPTbdWpu', '$2y$10$l7ZteUSsuXONAafdcUvjIu3VJC9VTncWnoyo0mOL4KqqNqppYQZea', 'Ifan Maulana Saputra', NULL, NULL, 'L', NULL, NULL, NULL, 4),
(115, 1, 101, NULL, 'NafaSafinaMaharani@gmail.com', '$2y$10$XqW3AtNBBpNTgcoYKi80UeBhogJATPeZS3DksLkKDMC95tx1Rbu4q', '$2y$10$Fnp0N7rjCbIhgzBA42RZDelasuarrwNUnBnQcHSBIj8Nwb9KDG.ne', 'Nafa Safina Maharani', NULL, NULL, 'P', NULL, NULL, NULL, 4),
(116, 1, 102, NULL, 'BillyAhmadAlfino@gmail.com', '$2y$10$7cVyYZv2dacwoK2R.nw8VuU4NMVEYsrhznIfSb6pb3afe3rpschve', '$2y$10$xMpYqjrWvK58vhmDk0i4xeLZ3JCx5CxVCljq06LUeeU7AJ1IdWbom', 'Billy Ahmad Alfino', NULL, NULL, 'L', NULL, NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bani`
--

CREATE TABLE `bani` (
  `id_bani` int(11) NOT NULL,
  `nama_bani` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bani`
--

INSERT INTO `bani` (`id_bani`, `nama_bani`) VALUES
(1, 'sadinah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `banner`
--

CREATE TABLE `banner` (
  `id_banner` int(11) NOT NULL,
  `id_bani` int(11) NOT NULL,
  `keterangan_banner` varchar(200) DEFAULT NULL,
  `banner` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `banner`
--

INSERT INTO `banner` (`id_banner`, `id_bani`, `keterangan_banner`, `banner`) VALUES
(1, 1, 'DAFTAR EVALUASI PESERTA PENGENALAN KEHIDUPAN KAMPUS MAHASISWA BARU UNIVERSITAS MADURA TAHUN AKADEMIK 2018/2019', '1559977382.png'),
(2, 1, 'keterangan blabla balbla', '1559977418.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `tanggal_berita` date NOT NULL,
  `judul_berita` varchar(255) NOT NULL,
  `cover_berita` text NOT NULL,
  `isi_berita` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `id_admin`, `tanggal_berita`, `judul_berita`, `cover_berita`, `isi_berita`) VALUES
(5, 2, '2019-06-08', 'Undangan halal bihalal Bani Sadinah 2019 M / 1440H', '1559997840.png', '<p style=\"margin-left:0cm; margin-right:0cm\"><strong><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Monotype Corsiva&quot;\">Assalamualaikum Wr. Wb</span></span></strong></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Denga&nbsp;memohon rahmat dan ridho Allah Swt. Kami mengharapkan dengan hormat atas kehadiran Saudara/i sekeluarga pada : </span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Hari &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Ahad</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Tanggal &nbsp;&nbsp;&nbsp; : 9 Juni 2019 M / 5 Syawal 1440 H.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Jam &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 08.00 wib.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Tempat &nbsp;&nbsp;&nbsp;&nbsp; : Rumah Abdur Rahman (Kaduara Timur)</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Keperluan&nbsp; : Halal Bi Halal Keluarga Besar Bani Sadinah</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Demikian undangan kami, atas perhatian dan kehadirannya kami sampaikan terima kasih.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><strong><span style=\"font-family:&quot;Monotype Corsiva&quot;\">Wassalamualaikum Wr. Wb</span></strong></span></span></p>'),
(6, 2, '2021-04-30', 'Undangan halal bihalal Bani Sadinah 2021 M / 1442H', '1619788938.png', '<p>testing</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto`
--

CREATE TABLE `foto` (
  `id_foto` int(11) NOT NULL,
  `id_album` int(11) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `foto`
--

INSERT INTO `foto` (`id_foto`, `id_album`, `foto`) VALUES
(2, 1, '1559996454.jpg'),
(3, 1, '1559996439.jpg'),
(4, 1, '1559996425.jpg'),
(5, 1, '1559996539.jpg'),
(6, 1, '1559996588.jpg'),
(7, 1, '1559996663.jpg'),
(8, 1, '1559996673.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `info`
--

CREATE TABLE `info` (
  `id_info` int(10) NOT NULL,
  `id_bani` int(11) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `judul_info` varchar(200) NOT NULL,
  `tanggal_info` date NOT NULL,
  `info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `info`
--

INSERT INTO `info` (`id_info`, `id_bani`, `id_admin`, `judul_info`, `tanggal_info`, `info`) VALUES
(1, 1, 1, 'hai', '2019-01-15', 'hai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan`
--

CREATE TABLE `laporan` (
  `id_laporan` int(11) NOT NULL,
  `id_bani` int(11) NOT NULL,
  `keterangan_laporan` text NOT NULL,
  `laporan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `laporan`
--

INSERT INTO `laporan` (`id_laporan`, `id_bani`, `keterangan_laporan`, `laporan`) VALUES
(1, 1, 'laporan keuangan Bani Sadinah 2018', '1559997922.PNG'),
(2, 1, 'Laporan keuangan 2019', '1560053037.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `panggilan`
--

CREATE TABLE `panggilan` (
  `id_panggilan` int(11) NOT NULL,
  `selisih` int(11) NOT NULL,
  `nama_pangilan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id_album`);

--
-- Indeks untuk tabel `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`),
  ADD KEY `id_ayah` (`id_ayah`),
  ADD KEY `id_ibu` (`id_ibu`),
  ADD KEY `id_bani` (`id_bani`);

--
-- Indeks untuk tabel `bani`
--
ALTER TABLE `bani`
  ADD PRIMARY KEY (`id_bani`);

--
-- Indeks untuk tabel `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id_banner`),
  ADD KEY `id_bani` (`id_bani`);

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indeks untuk tabel `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id_foto`),
  ADD KEY `id_album` (`id_album`);

--
-- Indeks untuk tabel `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id_info`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_admin_2` (`id_admin`),
  ADD KEY `id_bani` (`id_bani`);

--
-- Indeks untuk tabel `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id_laporan`),
  ADD KEY `id_bani` (`id_bani`);

--
-- Indeks untuk tabel `panggilan`
--
ALTER TABLE `panggilan`
  ADD PRIMARY KEY (`id_panggilan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `album`
--
ALTER TABLE `album`
  MODIFY `id_album` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT untuk tabel `bani`
--
ALTER TABLE `bani`
  MODIFY `id_bani` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `banner`
--
ALTER TABLE `banner`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `foto`
--
ALTER TABLE `foto`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `info`
--
ALTER TABLE `info`
  MODIFY `id_info` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `panggilan`
--
ALTER TABLE `panggilan`
  MODIFY `id_panggilan` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `anggota`
--
ALTER TABLE `anggota`
  ADD CONSTRAINT `anggota_ibfk_1` FOREIGN KEY (`id_bani`) REFERENCES `bani` (`id_bani`),
  ADD CONSTRAINT `anggota_ibfk_2` FOREIGN KEY (`id_ayah`) REFERENCES `anggota` (`id_anggota`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `anggota_ibfk_3` FOREIGN KEY (`id_ibu`) REFERENCES `anggota` (`id_anggota`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ketidakleluasaan untuk tabel `banner`
--
ALTER TABLE `banner`
  ADD CONSTRAINT `banner_ibfk_1` FOREIGN KEY (`id_bani`) REFERENCES `bani` (`id_bani`);

--
-- Ketidakleluasaan untuk tabel `foto`
--
ALTER TABLE `foto`
  ADD CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`id_album`) REFERENCES `album` (`id_album`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `info`
--
ALTER TABLE `info`
  ADD CONSTRAINT `info_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `laporan`
--
ALTER TABLE `laporan`
  ADD CONSTRAINT `laporan_ibfk_1` FOREIGN KEY (`id_bani`) REFERENCES `bani` (`id_bani`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
