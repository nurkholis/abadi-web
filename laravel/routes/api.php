<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('banner/{id_bani}', 'Api\BannerController@get');
Route::get('anggota/{id_bani}', 'Api\AnggotaController@get');
Route::get('album/{id_bani}', 'Api\AlbumController@get');
Route::get('foto/{id_bani}', 'Api\FotoController@get');
Route::get('berita/{id_bani}', 'Api\BeritaController@get');
Route::get('laporan/{id_bani}', 'Api\LaporanController@get');
Route::get('bagan/{id_anggota}', 'Api\BaganController@get');

