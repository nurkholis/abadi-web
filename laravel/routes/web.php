<?php
Route::prefix('/')->group( function() {
    Route::get('bagan/{id_anggota}', 'BaganController@bagan')->name('bagan');
});

Route::prefix('/admin')->group( function() {

    Route::get('', 'WebAdmin\HomeController@index')->name('admin.home');
    Route::get('login', 'AuthAdmin\LoginController@showloginForm')->name('admin.login');
    Route::post('login', 'AuthAdmin\LoginController@login')->name('admin.login.submit');
    Route::get('logout', function(){
        Auth::logout();
        Session::flush();
        return redirect(route('admin.login'));
    })->name('admin.logout'); 

    Route::prefix('album')->group( function() {
        Route::get('/', 'WebAdmin\AlbumController@index')->name('admin.album.index');
        Route::get('/dataTable', 'WebAdmin\AlbumController@dataTable')->name('admin.album.dataTable');
        Route::get('/form', 'WebAdmin\AlbumController@create')->name('admin.album.create');
        Route::get('/form/{id}', 'WebAdmin\AlbumController@edit')->name('admin.album.edit');
        Route::post('/', 'WebAdmin\AlbumController@store')->name('admin.album.store');
        Route::get('/delete/{id}', 'WebAdmin\AlbumController@destroy')->name('admin.album.delete');
        Route::put('/{id}', 'WebAdmin\AlbumController@update')->name('admin.album.put');
    });

    Route::prefix('foto')->group( function() {
        Route::get('/', 'WebAdmin\FotoController@index')->name('admin.foto.index');
        Route::get('/dataTable', 'WebAdmin\FotoController@dataTable')->name('admin.foto.dataTable');
        Route::get('/form', 'WebAdmin\FotoController@create')->name('admin.foto.create');
        Route::get('/form/{id}', 'WebAdmin\FotoController@edit')->name('admin.foto.edit');
        Route::post('/', 'WebAdmin\FotoController@store')->name('admin.foto.store');
        Route::get('/delete/{id}', 'WebAdmin\FotoController@destroy')->name('admin.foto.delete');
        Route::put('/{id}', 'WebAdmin\FotoController@update')->name('admin.foto.put');
    });

    Route::prefix('anggota')->group( function() {
        Route::get('/', 'WebAdmin\AnggotaController@index')->name('admin.anggota.index');
        Route::get('/detail/{id}', 'WebAdmin\AnggotaController@detail')->name('admin.anggota.detail');
        Route::get('/dataTabel', 'WebAdmin\AnggotaController@dataTable')->name('admin.anggota.dataTable');
        Route::get('/form', 'WebAdmin\AnggotaController@create')->name('admin.anggota.create');
        Route::get('/form/{id}/{generasi}', 'WebAdmin\AnggotaController@edit')->name('admin.anggota.edit');
        Route::post('/', 'WebAdmin\AnggotaController@store')->name('admin.anggota.store');
        Route::get('/delete/{id}', 'WebAdmin\AnggotaController@destroy')->name('admin.anggota.delete');
        Route::put('/{id}', 'WebAdmin\AnggotaController@update')->name('admin.anggota.put');
    });

    Route::prefix('berita')->group( function() {
        Route::get('/', 'WebAdmin\BeritaController@index')->name('admin.berita.index');
        Route::get('/dataTabel', 'WebAdmin\BeritaController@dataTable')->name('admin.berita.dataTable');
        Route::get('/form', 'WebAdmin\BeritaController@create')->name('admin.berita.create');
        Route::get('/form/{id}', 'WebAdmin\BeritaController@edit')->name('admin.berita.edit');
        Route::post('/', 'WebAdmin\BeritaController@store')->name('admin.berita.store');
        Route::get('/delete/{id}', 'WebAdmin\BeritaController@destroy')->name('admin.berita.delete');
        Route::put('/{id}', 'WebAdmin\BeritaController@update')->name('admin.berita.put');
    });

    Route::prefix('banner')->group( function() {
        Route::get('/', 'WebAdmin\BannerController@index')->name('admin.banner.index');
        Route::get('/dataTabel', 'WebAdmin\BannerController@dataTable')->name('admin.banner.dataTable');
        Route::get('/form', 'WebAdmin\BannerController@create')->name('admin.banner.create');
        Route::get('/form/{id}', 'WebAdmin\BannerController@edit')->name('admin.banner.edit');
        Route::post('/', 'WebAdmin\BannerController@store')->name('admin.banner.store');
        Route::get('/delete/{id}', 'WebAdmin\BannerController@destroy')->name('admin.banner.delete');
        Route::put('/{id}', 'WebAdmin\BannerController@update')->name('admin.banner.put');
    });

    Route::prefix('laporan')->group( function() {
        Route::get('/', 'WebAdmin\LaporanController@index')->name('admin.laporan.index');
        Route::get('/dataTabel', 'WebAdmin\LaporanController@dataTable')->name('admin.laporan.dataTable');
        Route::get('/form', 'WebAdmin\LaporanController@create')->name('admin.laporan.create');
        Route::get('/form/{id}', 'WebAdmin\LaporanController@edit')->name('admin.laporan.edit');
        Route::post('/', 'WebAdmin\LaporanController@store')->name('admin.laporan.store');
        Route::get('/delete/{id}', 'WebAdmin\LaporanController@destroy')->name('admin.laporan.delete');
        Route::put('/{id}', 'WebAdmin\LaporanController@update')->name('admin.laporan.put');
    });

    Route::prefix('profil')->group( function() {
        Route::get('/', 'WebAdmin\AdminController@index')->name('admin.admin.index');
        Route::put('/', 'WebAdmin\AdminController@update')->name('admin.admin.put');
        Route::put('/pw', 'WebAdmin\AdminController@updatePw')->name('admin.admin.putPw');
    });

});


