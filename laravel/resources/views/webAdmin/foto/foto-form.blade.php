@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} foto</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.foto.put', $edit->id_foto) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.foto.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group row">
    <label class="col-sm-2 col-form-label">album</label>
    <div class="col-sm-7">
        <select class="form-control" name="id_album">
            @if (isset($edit))
                @foreach ($album as $item)
                    <option value="{{ $item->id_album }}" {{ $item->id_album == $edit->id_album ? 'selected' : '' }} >{{ $item->nama_album }}</option>
                @endforeach
            @else
                @foreach ($album as $item)
                    <option value="{{ $item->id_album }}"> {{ $item->nama_album }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label"> foto</label>
    <div class="col-sm-4">
        <label class="custom-file">
            <input type="file" class="custom-file-input"  id="input-foto" name="foto" accept=".jpg,.png">
            <span id="img-name" class="custom-file-control"></span></label>
    </div>
    <div class="col-sm-3 text-center">
        <img src="" id="prev-foto" class="img-preview"> 
        @if(isset($edit))
            <span>baru</span>
        @endif   
    </div>
    <div class="col-sm-3 text-center">
        @if(isset($edit))
            <img src="{{ asset('/public/images/foto').'/'.$edit->foto }}" alt="" class="img-preview">
            <span>lama</span>
        @endif    
    </div> 
</div>

                                <div class="float-right">
                                    <a href="{{ route('admin.foto.index') }}" class="btn btn-danger ">Batal</a>
                                    <button type="submit" class="btn btn-primary ">Simpan</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
$(document).ready(function () {

    $("#input-foto").change(function() {
        var input = this;
        if (input.files && input.files[0]) {
            console.log(input.files[0].name);
            $("#img-name").html(input.files[0].name)
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    });
}); 

</script>
        
@endsection