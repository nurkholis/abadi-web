@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} sejarah</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger alert-with-icon" data-notify="container">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="fa fa-close"></i>
                                </button>
                                <span data-notify="message">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </span>
                            </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.sejarah.put', $edit->id_sejarah) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.sejarah.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group">
    <div class="form-group bmd-form-group text-capitalize">
        <textarea class="form-control" name="isi_sejarah" id="isi-sejarah" rows="10" required>@if( isset($edit) ) {{ $edit->isi_sejarah }} @else {{ old('isi_sejarah') }} @endif</textarea>
    </div>
</div>

                                <div class="float-right">
                                    <a href="{{ route('admin.sejarah.index') }}" class="btn btn-danger ">Batal</a>
                                    <button type="submit" class="btn btn-primary ">Simpan</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script src="{{ asset('public/assets/vendor/ckeditor/ckeditor.js') }}"></script>
<script>
$(document).ready(function () {
}); 

</script>
        
@endsection