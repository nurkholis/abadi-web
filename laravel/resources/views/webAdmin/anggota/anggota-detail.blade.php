@extends('layouts.admin')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
    @php
        use Config\Kholis as Helper;
    @endphp

    <div class="container">
        <section class="chart_section">
            <div class="row">
                
                    <div class="col-md-8 mx-auto">
                        <div class="user_area card card-shadow mb-4">
                            <div class="profile_wid_user">
                                <div class="profile-widget-head">
                                    <h3>{{ $item->nama_kader }}</h3>
                                    <i>{{ $item->nama_rayon }}, {{ $item->nama_komisariat }}</i>
                                    <span><img src="{{ asset('/public/images/kader'). '/' }}{{ $item->foto }}" alt=""></span>
                                </div>
                                <blockquote> " {{ $item->motto }} " </blockquote>
                                @if ($item->gender == 'L')
                                    <span class="badge badge-pill badge-success">Laki-laki</span>
                                @else
                                <span class="badge badge-pill badge-warning">Perempuan</span>
                                @endif
                                <p><i class="fa fa-map-marker"></i> {{ $item->alamat }}</p>
                                <p><i class="fa fa-calendar"></i> {{ $item->tempat_lahir }}, {{ Helper::tanggal( $item->tanggal_lahir) }}</p>
                                <p><i class="fa fa-envelope-o"></i> {{ $item->email }}</p>
                                <p><i class="fa fa-phone"></i> {{ $item->telepon }}</p>
                                <p><i class="fa fa-vcard"></i> {{ $item->tahun_masuk }}</p>
                            </div><!-- My Profile Widget -->
                        </div>
                    </div>

            </div>
        </section>
    </div>

@endsection