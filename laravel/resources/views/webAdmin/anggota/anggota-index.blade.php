@extends('layouts.admin')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')

    <div class="container">
        <section class="chart_section">
            <div class="row">
                <div class="col-md-12 mb-4 d-flex align-items-stretch">
                    <div class="widthfull card card-shadow">
                        <div class="card-header">
                            <div class="card-title">
                               <span> Data Anggota</span>
                               <a href="{{ route('admin.anggota.create') }}" class="btn btn-primary float-right">Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="bd-example table_style">

                                <table id="table" class="table table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Pasangan</th>
                                            <th>Ayah</th>
                                            <th>Ibu</th>
                                            <th>Tetala</th>
                                            <th>Generasi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

@endsection

@section('js')
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.anggota.dataTable') }}",
            columns: [
                {data: 'nama'},
                {data: 'pasangan'},
                {data: 'ayah'},
                {data: 'ibu'},
                {data: 'tetala'},
                {data: 'generasi'},
                {data: 'action'},
            ]
        });
    </script>
@endsection