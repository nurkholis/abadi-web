@extends('layouts.admin')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css" integrity="sha512-3//o69LmXw00/DZikLz19AetZYntf4thXiGYJP6L49nziMIhp6DVrwhkaQ9ppMSy8NWXfocBwI3E8ixzHcpRzw==" crossorigin="anonymous" />
@endsection

@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mx-auto">
                    <div class="card">
                        <div class="card-header card-header-success">
                            <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} anggota</h4>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger alert-with-icon" data-notify="container">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <i class="fa fa-close"></i>
                                    </button>
                                    <span data-notify="message">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </span>
                                </div>
                            @endif
                            @if (isset($edit))
                                <form method="POST" action="{{ route('admin.anggota.put', $edit->id_anggota) }}"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_method" value="PUT">
                                @else
                                    <form method="POST" action="{{ route('admin.anggota.store') }}"
                                        enctype="multipart/form-data">
                            @endif
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    {{-- form kiri --}}
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">nama anggota</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="nama" @if (isset($edit)) value="{{ $edit->nama }}" @else value="{{ old('nama') }}" @endif required>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">gender</label>
                                        <div class="col-sm-3">
                                            <select class="form-control" name="gender">
                                                <option value="L" @if (isset($edit))  @if ($edit->gender=='L') selected @endif
                                                    @endif>Laki-laki</option>
                                                <option value="P" @if (isset($edit))  @if ($edit->gender=='P') selected @endif
                                                    @endif>Perempuan</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">pasangan</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="pasangan" @if (isset($edit)) value="{{ $edit->pasangan }}" @else value="{{ old('pasangan') }}" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">ayah</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="id_ayah">
                                                <option value="">-- Pilih Ayah --</option>
                                                @if (isset($edit))
                                                    @foreach ($anggota_cowok as $item)
                                                        <option value="{{ $item->id_anggota }}"
                                                            {{ $item->id_anggota == $edit->id_ayah ? 'selected' : '' }}>
                                                            {{ $item->nama }}</option>
                                                    @endforeach
                                                @else
                                                    @foreach ($anggota_cowok as $item)
                                                        <option value="{{ $item->id_anggota }}"> {{ $item->nama }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">ibu</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="id_ibu">
                                                <option value="">-- Pilih Ibu --</option>
                                                @if (isset($edit))
                                                    @foreach ($anggota_cewek as $item)
                                                        <option value="{{ $item->id_anggota }}"
                                                            {{ $item->id_anggota == $edit->id_ibu ? 'selected' : '' }}>
                                                            {{ $item->nama }}</option>
                                                    @endforeach
                                                @else
                                                    @foreach ($anggota_cewek as $item)
                                                        <option value="{{ $item->id_anggota }}"> {{ $item->nama }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- akhir form kiri --}}
                                </div>
                                <div class="col-md-6">
                                    {{-- form kanan --}}
                                    

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">email</label>
                                        <div class="col-sm-7">
                                            <input type="email" class="form-control" name="email" @if (isset($edit)) value="{{ $edit->email }}" @else value="{{ old('email') }}" @endif>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">tanggal lahir</label>
                                        <div class="col-sm-4">
                                            <input type="date" class="form-control" name="tanggal_lahir" @if (isset($edit)) value="{{ $edit->tanggal_lahir }}" @else value="{{ old('tanggal_lahir') }}" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">alamat</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="alamat" @if (isset($edit)) value="{{ $edit->alamat }}" @else value="{{ old('alamat') }}" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">telepon</label>
                                        <div class="col-sm-5">
                                            <input type="number" class="form-control" name="telepon" @if (isset($edit)) value="{{ $edit->telepon }}" @else value="{{ old('telepon') }}" @endif >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">foto</label>
                                        <div class="col-sm-4">
                                            <label class="custom-file">
                                                <input type="file" class="custom-file-input" id="input-foto" name="foto"
                                                    accept=".jpg,.png">
                                                <span id="img-name" class="custom-file-control"></span></label>
                                        </div>
                                        <div class="col-sm-3 text-center">
                                            <img src="" id="prev-foto" class="img-preview">
                                            @if (isset($edit))
                                                <span>baru</span>
                                            @endif
                                        </div>
                                        <div class="col-sm-3 text-center">
                                            @if (isset($edit))
                                                <img src="{{ asset('/public/images/anggota') . '/' . $edit->foto }}" alt=""
                                                    class="img-preview">
                                                <span>lama</span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- akhir form kanan --}}
                                </div>
                            </div>

                            <div class="float-right">
                                <a href="{{ route('admin.anggota.index') }}" class="btn btn-danger ">Batal</a>
                                <button type="submit" class="btn btn-primary ">Simpan</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            
            $("select[name=id_ibu]").select2();
            $("select[name=id_ayah]").select2();

            $("#input-foto").change(function() {
                var input = this;
                if (input.files && input.files[0]) {
                    console.log(input.files[0].name);
                    $("#img-name").html(input.files[0].name)
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#prev-foto').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            });
        });

    </script>

@endsection
