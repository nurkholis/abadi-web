@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} banner</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.banner.put', $edit->id_banner) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.banner.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group row">
    <label class="col-sm-2 col-form-label">keterangan banner</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="keterangan_banner"
        @if( isset($edit) ) value="{{ $edit->keterangan_banner }}" @else value="{{ old('keterangan_banner') }}" @endif>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">banner</label>
    <div class="col-sm-4">
        <label class="custom-file">
            <input type="file" class="custom-file-input"  id="input-banner" name="banner" accept=".jpg,.png">
            <span id="img-name" class="custom-file-control"></span></label>
    </div>
    <div class="col-sm-3 text-center">
        <img src="" id="prev-banner" class="img-preview"> 
        @if(isset($edit))
            <span>baru</span>
        @endif   
    </div>
    <div class="col-sm-3 text-center">
        @if(isset($edit))
            <img src="{{ asset('/public/images/banner').'/'.$edit->banner }}" alt="" class="img-preview">
            <span>lama</span>
        @endif    
    </div> 
</div>

                                <div class="float-right">
                                    <a href="{{ route('admin.banner.index') }}" class="btn btn-danger ">Batal</a>
                                    <button type="submit" class="btn btn-primary ">Simpan</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
$(document).ready(function () {

    $("#input-banner").change(function() {
        var input = this;
        if (input.files && input.files[0]) {
            console.log(input.files[0].name);
            $("#img-name").html(input.files[0].name)
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-banner').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    });
}); 

</script>
        
@endsection