@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} album</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.album.put', $edit->id_album) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.album.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group row">
    <label class="col-sm-2 col-form-label">nama album</label>
    <div class="col-sm-7">
        <input type="text" class="form-control" name="nama_album"
        @if( isset($edit) ) value="{{ $edit->nama_album }}" @else value="{{ old('nama_album') }}" @endif required>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">cover  album</label>
    <div class="col-sm-4">
        <label class="custom-file">
            <input type="file" class="custom-file-input"  id="input-foto" name="cover_album" accept=".jpg,.png">
            <span id="img-name" class="custom-file-control"></span></label>
    </div>
    <div class="col-sm-3 text-center">
        <img src="" id="prev-foto" class="img-preview"> 
        @if(isset($edit))
            <span>baru</span>
        @endif   
    </div>
    <div class="col-sm-3 text-center">
        @if(isset($edit))
            <img src="{{ asset('/public/images/album').'/'.$edit->cover_album }}" alt="" class="img-preview">
            <span>lama</span>
        @endif    
    </div> 
</div>

                                <div class="float-right">
                                    <a href="{{ route('admin.album.index') }}" class="btn btn-danger ">Batal</a>
                                    <button type="submit" class="btn btn-primary ">Simpan</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script>
$(document).ready(function () {

    $("#input-foto").change(function() {
        var input = this;
        if (input.files && input.files[0]) {
            console.log(input.files[0].name);
            $("#img-name").html(input.files[0].name)
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    });
}); 

</script>
        
@endsection