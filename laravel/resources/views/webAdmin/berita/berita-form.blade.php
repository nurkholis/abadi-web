@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} berita</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.berita.put', $edit->id_berita) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.berita.store') }}" enctype="multipart/form-data">
                        @endif
                                {{ csrf_field() }}

<div class="form-group row">
    <label class="col-sm-2 col-form-label">judul berita</label>
    <div class="col-sm-7">
        <input type="text" class="form-control" name="judul_berita"
        @if( isset($edit) ) value="{{ $edit->judul_berita }}" @else value="{{ old('judul_berita') }}" @endif required>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">cover  berita</label>
    <div class="col-sm-4">
        <label class="custom-file">
            <input type="file" class="custom-file-input"  id="input-foto" name="cover_berita" accept=".jpg,.png">
            <span id="img-name" class="custom-file-control"></span></label>
    </div>
    <div class="col-sm-1 text-center">
        <img src="" id="prev-foto" class="img-preview"> 
        @if(isset($edit))
            <span>baru</span>
        @endif   
    </div>
    <div class="col-sm-1 text-center">
        @if(isset($edit))
            <img src="{{ asset('/public/images/berita/cover').'/'.$edit->cover_berita }}" alt="" class="img-preview">
            <span>lama</span>
        @endif    
    </div> 
</div>

<div class="form-group">
    <div class="form-group bmd-form-group text-capitalize">
        <label class="bmd-label-floating">isi berita</label>
        <textarea class="form-control" name="isi_berita" id="isi-berita" rows="10" required>@if( isset($edit) ) {{ $edit->isi_berita }} @else {{ old('isi_berita') }} @endif</textarea>
    </div>
</div>

                                <div class="float-right">
                                    <a href="{{ route('admin.berita.index') }}" class="btn btn-danger ">Batal</a>
                                    <button type="submit" class="btn btn-primary ">Simpan</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script src="{{ asset('public/assets/vendor/ckeditor/ckeditor.js') }}"></script>
<script>
$(document).ready(function () {
    CKEDITOR.replace( 'isi-berita' );
    $("#input-foto").change(function() {
        var input = this;
        if (input.files && input.files[0]) {
            console.log(input.files[0].name);
            $("#img-name").html(input.files[0].name)
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#prev-foto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    });
}); 

</script>
        
@endsection