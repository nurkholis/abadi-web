<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link rel="shortcut icon" type="image/x-icon" href="/public/favicon.ico">
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <link href="/public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/public/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/public/assets/css/ionicons.css" rel="stylesheet" type="text/css">
    <link href="/public/assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="/public/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <link href="/public/assets/css/style.css" rel="stylesheet">
    <link href="/public/assets/css/responsive.css" rel="stylesheet">
</head>

<body class="bg_darck">
    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="logo">
                    <a href="#">
                        <strong class="logo_icon">
                            <img alt="" src="{{ asset('public/images/web/logo_small.png') }}">
                        </strong>
                        <span class="logo-default">
                            <img alt="" src="{{ asset('public/images/web/logo_small.png') }}">
                        </span>
                    </a>
                </div>
                <div class="login-form">
                    <form class="login-form" method="POST" action="{{ route('admin.login.submit') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="bmd-label-floating">Username</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="bmd-label-floating">Password</label>
                            <input type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Log in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/public/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/assets/js/popper.min.js"></script>
    <script type="text/javascript" src="/public/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/public/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/public/assets/js/custom.js" type="text/javascript"></script>
</body>
</html>