<?php

namespace App\Transformers;

use App\Anggota;
use League\Fractal\TransformerAbstract;

class AnggotaTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['ayah', 'ibu'];
    public function transform(Anggota $Model)
    {
        return [
            'id_anggota' => $Model->id_anggota,
            'id_bani' => $Model->id_bani,
            'id_ibu' => $Model->id_ibu,
            'id_ayah' => $Model->id_ayah,
            'email' => $Model->email,
            'nama' => $Model->nama,
            'pasangan' => $Model->pasangan,
            'gender' => $Model->gender,
            'generasi' => $Model->generasi,
            'foto' => "https://avatars.dicebear.com/api/" . ($Model->gender == "L" ? "male" : "female")  . "/" . $Model->nama . ".svg",
        ];
    }

    public function includeAyah(Anggota $model)
    {
        $data = Anggota::find($model->id_ayah);
        if($data)
        {
            return $this->item($data, new AnggotaTransformer);
        }
    }

    public function includeIbu(Anggota $model)
    {
        $data = Anggota::find($model->id_ibu);
        if($data)
        {
            return $this->item($data, new AnggotaTransformer);
        }
    }
}