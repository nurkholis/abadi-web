<?php

namespace App\Transformers;

use App\Album;
use League\Fractal\TransformerAbstract;

class AlbumTransformer extends TransformerAbstract
{
    public function transform(Album $data)
    {
        return [
            'id_album' => $data->id_album,
            'nama_album' => $data->nama_album,
            'cover_album' => asset("public/images/album/".$data->cover_album),
        ];
    }
}