<?php

namespace App\Transformers;

use App\Laporan;
use League\Fractal\TransformerAbstract;

class LaporanTransformer extends TransformerAbstract
{
    public function transform(Laporan $data)
    {
        return [
            'id_laporan' => $data->id_laporan,
            'keterangan_laporan' => $data->keterangan_laporan,
            'laporan' => asset("public/images/laporan/".$data->laporan),
        ];
    }
}