<?php

namespace App\Transformers;

use App\Berita;
use League\Fractal\TransformerAbstract;
use App\Admin;
use App\Transformers\AdminTransformer;

class BeritaTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['admin'];

    public function transform(Berita $data)
    {
        return [
            'id_berita' => $data->id_berita,
            'judul_berita' => $data->judul_berita,
            'cover_berita' => asset("public/images/berita/cover/".$data->cover_berita),
            'tanggal_berita' => $data->tanggal_berita,
            'isi_berita' => $data->isi_berita,
        ];
    }

    public function includeAdmin(Berita $model)
    {
        $data = Admin::find($model->id_admin);
        return $this->item($data, new AdminTransformer);
    }
}