<?php

namespace App\Transformers;

use App\Banner;
use League\Fractal\TransformerAbstract;

class BannerTransformer extends TransformerAbstract
{
    public function transform(Banner $Model)
    {
        return [
            'id_banner' => $Model->id_banner,
            'keterangan_banner' => $Model->keterangan_banner,
            'banner' => $Model->banner,
        ];
    }
}