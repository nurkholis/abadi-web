<?php

namespace App\Transformers;

use App\Foto;
use League\Fractal\TransformerAbstract;
use App\Album;
use App\Transformers\AlbumTransformer;

class FotoTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['album'];

    public function transform(Foto $data)
    {
        return [
            'id_foto' => $data->id_foto,
            'foto' => asset("public/images/foto/".$data->foto),
        ];
    }

    public function includeAlbum(Foto $model)
    {
        $data = Album::find($model->id_album);
        return $this->item($data, new AlbumTransformer);
    }
}