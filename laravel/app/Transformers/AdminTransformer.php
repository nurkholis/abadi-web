<?php

namespace App\Transformers;

use App\Admin;
use League\Fractal\TransformerAbstract;

class AdminTransformer extends TransformerAbstract
{
    public function transform(Admin $data)
    {
        return [
            'id_admin' => $data->id_admin,
            'nama_admin' => $data->nama_admin,
        ];
    }
}