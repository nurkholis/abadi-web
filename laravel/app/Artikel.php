<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Artikel extends Authenticatable
{
    use Notifiable;

    protected $guard = 'artikel';
    protected $table = 'artikel';
    protected $primaryKey = 'id_artikel';
    public $timestamps = false;
    protected $guarded = [];
}
