<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Anggota;

class BaganController extends Controller
{
    public function bagan($id_anggota)
    {
        $array_turunan1 = [];
        $array_turunan2 = [];
        $array_turunan3 = [];
        $array_turunan4 = [];
        $array_turunan5 = [];
        $anggota = Anggota::find($id_anggota);
        $turunan1 = $anggota->where('id_ayah', '=', $id_anggota)
            ->orWhere('id_ibu', '=', $id_anggota)
            ->get();
        foreach ($turunan1 as $a => $value) {
            $turunan2 = Anggota::where('id_ayah', '=', $value->id_anggota)
                ->orWhere('id_ibu', '=', $value->id_anggota)
                ->get();
            foreach ($turunan2 as $b => $value_turunan2) {
                $turunan3 = Anggota::where('id_ayah', '=', $value_turunan2->id_anggota)
                    ->orWhere('id_ibu', '=', $value_turunan2->id_anggota)
                    ->get();
                foreach ($turunan3 as $b => $value_turunan3) {
                    // turunan 4
                    $turunan4 = Anggota::where('id_ayah', '=', $value_turunan3->id_anggota)
                        ->orWhere('id_ibu', '=', $value_turunan3->id_anggota)
                        ->get();
                    foreach ($turunan4 as $c => $value_turunan4) {
                        // turunan 5
                        $turunan5 = Anggota::where('id_ayah', '=', $value_turunan4->id_anggota)
                            ->orWhere('id_ibu', '=', $value_turunan4->id_anggota)
                            ->get();
                        foreach ($turunan5 as $c => $value_turunan5) {
                            array_push($array_turunan5, [
                                "id" => $value_turunan5->id_anggota,
                                "name" => $value_turunan5->nama,
                                "title" => "Generasi ke-" . $value_turunan5->generasi,
                                "relationship" => [
                                    "children_num" => "0"
                                ],
                                "children" => [],
                            ]);
                        }
                        array_push($array_turunan4, [
                            "id" => $value_turunan4->id_anggota,
                            "name" => $value_turunan4->nama,
                            "title" => "Generasi ke-" . $value_turunan4->generasi,
                            "relationship" => [
                                "children_num" => "0"
                            ],
                            "children" => $array_turunan5,
                        ]);
                        $array_turunan5 = [];
                    }
                    array_push($array_turunan3, [
                        "id" => $value_turunan3->id_anggota,
                        "name" => $value_turunan3->nama,
                        "title" => "Generasi ke-" . $value_turunan3->generasi,
                        "relationship" => [
                            "children_num" => "0"
                        ],
                        "children" => $array_turunan4,
                    ]);
                    $array_turunan4 = [];
                }
                array_push($array_turunan2, [
                    "id" => $value_turunan2->id_anggota,
                    "name" => $value_turunan2->nama,
                    "title" => "Generasi ke-" . $value_turunan2->generasi,
                    "relationship" => [
                        "children_num" => "0"
                    ],
                    "children" => $array_turunan3,
                ]);
                $array_turunan3 = [];
            }
            array_push($array_turunan1, [
                "id" => $value->id_anggota,
                "name" => $value->nama,
                "title" => "Generasi ke-" . $value->generasi,
                "relationship" => [
                    "children_num" => "0"
                ],
                "children" => $array_turunan2,
            ]);
            $array_turunan2 = [];
        }
        $data = [
            "id" => $anggota->id_anggota,
            "name" => $anggota->nama,
            "title" => "Generasi ke-" . $anggota->generasi,
            "relationship" => [
                "children_num" => "0"
            ],
            "children" => $array_turunan1,
        ];
        // dd($data);
        $anggota = Anggota::find($id_anggota);
        return view('bagan', ['anggota' => $anggota, "data" => $data]);
    }
}
