<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use Illuminate\Support\Facades\File;
use Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Admin $Admin)
    {   
        $param = [];
        $data = $Admin->find(Auth::guard('admin')->user()->id_admin);
        //dd($data);
        return view('webAdmin.admin.admin-index', ['edit' => $data]);
    }
    public function update(Request $request, Admin $Admin)
    {
        $this->validate($request, [
            'nama_admin' => 'required|min:3',
        ]);
        $old = $Admin->find(Auth::guard('admin')->user()->id_admin);
        $Admin = $Admin->find(Auth::guard('admin')->user()->id_admin);

        if( $old->email != $request->email ){
            $this->validate($request, [
                'email' => 'required|unique:admin'
            ]);
            $Admin->email = $request->email;
            $Admin->api_token = bcrypt($request->email);
        }
        $Admin->nama_admin = $request->get('nama_admin', $Admin->nama_admin);
        $Admin->save();
        return redirect(route('admin.admin.index'));
    }
    public function updatePw(Request $request, Admin $Admin)
    {
        $this->validate($request, [
            'password_baru' => 'required|min:6',
        ]);
        
        if( $request->password_baru != $request->password_konfirmasi ){
            return back()->withErrors(['password tidak konfirmasi tidak sama']);
        }

        $credential = [
            'email' => Auth::guard('admin')->user()->email,
            'password' => $request->password_lama,
        ];
        if(Auth::guard('admin')->attempt($credential, $request->member)){

            $Admin = $Admin->find(Auth::guard('admin')->user()->id_admin);
            $Admin->password = bcrypt($request->password_baru);
            $Admin->api_token = bcrypt($request->email);
            $Admin->nama_admin = $request->get('nama_admin', $Admin->nama_admin);
            $Admin->save();
            return redirect(route('admin.admin.index'));

        }else{
            return back()->withErrors(['password salah']);
        }
    }
    public function destroy($id, Sejarah $Sejarah)
    {
        $Sejarah = $Sejarah->find($id)->delete();
        return back();
    }
    
}
