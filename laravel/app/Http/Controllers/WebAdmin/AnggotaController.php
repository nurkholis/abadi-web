<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Anggota;
use App\Bani;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;
use Config\Kholis as Helper;
use Illuminate\Support\Facades\DB;

class AnggotaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {   
        return view('webAdmin.anggota.anggota-index');
    }
    public function detail($id, Anggota $Anggota)
    {
        $Anggota = $Anggota
            ->join('rayon', 'rayon.id_rayon', '=', 'anggota.id_rayon')
            ->join('komisariat', 'komisariat.id_komisariat', '=', 'rayon.id_komisariat')    
            ->find($id);
        return view('webAdmin.anggota.anggota-detail', [
            'item' => $Anggota
        ]) ;
    }
    public function dataTable(Request $request, Anggota $Anggota)
    {   
        $data = DB::select('SELECT a.*, ay.nama AS ayah, ab.nama AS ibu FROM anggota a LEFT JOIN anggota ay ON a.id_ayah = ay.id_anggota LEFT JOIN anggota ab ON a.id_ibu = ab.id_anggota ORDER BY a.generasi ASC');
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.anggota.edit', [$data->id_anggota, $data->generasi]) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.anggota.delete', $data->id_anggota) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->addColumn('tetala', function($data){
            $g = '';
            if( $data->gender == 'L'){
                $g = '<span class="badge badge-pill badge-success">Laki-laki</span>';
            }else{
                $g = '<span class="badge badge-pill badge-warning">Perempuan</span>';
            }
            return $g;
            // return $g . ', ' . Helper::tanggal($data->tanggal_lahir);
        })
        ->addColumn('foto', function ($data) {
            return '
                <a href="' . asset('/public/images/anggota').'/'.$data->foto .'" target="_blank">
                    <img src=" ' . asset('/public/images/anggota').'/'.$data->foto .' " height="50">
                </a>
            ';
        })
        ->rawColumns(['foto','action', 'tetala'])
        ->make(true);
    }
    public function create(Anggota $Anggota, Bani $Bani)
    {
        $Anggota_cowok = $Anggota->where('gender', 'L')->get();
        $Anggota_cewek = $Anggota->where('gender', 'P')->get();
        // $Bani = $Bani->all();
        return view('webAdmin.anggota.anggota-form', [
            'anggota_cowok' => $Anggota_cowok,
            'anggota_cewek' => $Anggota_cewek,
            // 'bani' => $Bani,
        ]);
    }
    public function store(Request $request, anggota $anggota )
    {
        $this->validate($request, [
            'nama' => 'required',
            'gender' => 'required',
        ]);
        $foto = null;
        if($request->foto)
        {
            $destinationPath = "public/images/anggota";
            $image = $request->file('foto');
            $foto = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $foto);
        }

        // get generasi
        

        $anggota = $anggota->create([
            'id_bani' => 1,
            'id_ayah' => $request->id_ayah,
            'id_ibu' => $request->id_ibu,
            'email' => $request->email != "" ? $request->email : str_replace(" ", "", $request->nama) . "@gmail.com",
            'password' => bcrypt(123123123),
            'api_token' => bcrypt($request->email),
            'nama' => $request->nama,
            'pasangan' => $request->pasangan,
            'gender' => $request->gender,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
            'generasi' => $request->generasi != "" ? $request->generasi : $this->getGenerasi($request->id_ayah, $request->id_ibu),
            'foto' => $foto,
        ]);

        return redirect(route('admin.anggota.index'));
    }

    public function getGenerasi($id_ayah, $id_ibu)
    {
        if($id_ayah != "" || $id_ayah != null)
        {
            $anggota = Anggota::where('id_anggota', '=', $id_ayah)->first();
            return $anggota->generasi + 1;
        }else
        {
            $anggota = Anggota::where('id_anggota', '=', $id_ibu)->first();
            return $anggota->generasi + 1;
        }
    }

    public function edit($id, $generasi, Anggota $Anggota)
    {
        $Anggota = $Anggota->find($id);
        $Anggota_cowok = $Anggota->where('gender', 'L')->where('id_anggota', '!=', $id)->where('generasi', '<', $generasi)->get();
        $Anggota_cewek = $Anggota->where('gender', 'P')->where('id_anggota', '!=', $id)->where('generasi', '<', $generasi)->get();
        // dd($Anggota);
        return view('webAdmin.anggota.anggota-form', [
            'edit'  => $Anggota,
            'anggota_cowok' => $Anggota_cowok,
            'anggota_cewek' => $Anggota_cewek,
            // 'bani' => $Bani,
            ]);
    }
    public function update(Request $request, $id, anggota $anggota)
    {
        $anggota = $anggota->find($id);
        if(!$request->foto == null ){
            $image_path = $destinationPath = "public/images/anggota/".$anggota->foto;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/anggota";
            $image = $request->file('foto');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $anggota->foto = $image_name;
        }
        $anggota->email = $request->get('email', $anggota->email);
        $anggota->id_ayah = $request->get('id_ayah', $anggota->id_ayah);
        $anggota->id_ibu = $request->get('id_ibu', $anggota->id_ibu);
        $anggota->nama = $request->get('nama', $anggota->nama);
        $anggota->pasangan = $request->get('pasangan', $anggota->pasangan);
        $anggota->tanggal_lahir = $request->get('tanggal_lahir', $anggota->tanggal_lahir);
        $anggota->gender = $request->get('gender', $anggota->gender);
        $anggota->alamat = $request->get('alamat', $anggota->alamat);
        $anggota->telepon = $request->get('telepon', $anggota->telepon);
        $anggota->generasi = $request->get('generasi', $anggota->generasi);
        $anggota->save();

        return redirect(route('admin.anggota.index'));
    }
    public function destroy($id, Anggota $Anggota)
    {
        $Anggota = $Anggota->find($id);

        $image_path = "public/images/anggota/".$Anggota->foto;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $Anggota = $Anggota->delete();
        return back();
    }
    
}
