<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Foto;
use App\Album;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;

class FotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        return view('webAdmin.foto.foto-index');
    }
    public function dataTable(Request $request, Foto $Foto)
    {   
        $data = $Foto
            ->join('album', 'album.id_album', '=', 'foto.id_album')
            ->orderBy('foto.id_foto', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.foto.edit', $data->id_foto) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.foto.delete', $data->id_foto) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->addColumn('foto', function ($data) {
            return '
                <a href="' . asset('/public/images/foto').'/'.$data->foto .'" target="_blank">
                    <img src=" ' . asset('/public/images/foto').'/'.$data->foto .' " height="50">
                </a>
            ';
        })
        ->rawColumns(['foto','action'])
        ->make(true);
    }
    public function create(Album $Album)
    {
        $Album = $Album->all();
        return view('webAdmin.foto.foto-form', [
            'album' => $Album
        ]);
    }
    public function store(Request $request, Foto $Foto )
    {
        $this->validate($request, [
            'id_album' => 'required',
            'foto' => 'required'
        ]);
        
        $destinationPath = "public/images/foto";
        $image = $request->file('foto');
        $foto = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $foto);

        $Foto = $Foto->create([
            'id_album' => $request->id_album,
            'foto' => $foto,
        ]);

        return redirect(route('admin.foto.index'));
    }
    public function edit($id, Foto $Foto, Album $Album)
    {
        $Album = $Album->all();
        $Foto = $Foto->find($id);
        return view('webAdmin.foto.foto-form', [
            'edit'  => $Foto,
            'album' => $Album,
            ]);
    }
    public function update(Request $request, $id, Foto $Foto)
    {
        $Foto = $Foto->find($id);
        if(!$request->foto  == null ){
            $image_path = $destinationPath = "public/images/foto/".$Foto->foto;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/foto";
            $image = $request->file('foto');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Foto->foto = $image_name;
        }
        $Foto->id_album = $request->get('id_album', $Foto->id_album);
        $Foto->save();

        return redirect(route('admin.foto.index'));
    }
    public function destroy($id, Foto $Foto)
    {
        $Foto = $Foto->find($id);
        $image_path = "public/images/foto/".$Foto->foto;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $Foto = $Foto->delete();
        return back();
    }
    
}
