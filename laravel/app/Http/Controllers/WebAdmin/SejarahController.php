<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sejarah;
use Illuminate\Support\Facades\File;
use Auth;

class SejarahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request, Sejarah $Sejarah)
    {   
        $Sejarah = $Sejarah->find(1);
        return view('webAdmin.sejarah.sejarah-index', [
            'edit'  => $Sejarah,
            ]);
    }
    public function update(Request $request, Sejarah $Sejarah)
    {
        $Sejarah = $Sejarah->find(1);
        $Sejarah->isi_sejarah = $request->isi_sejarah;
        $Sejarah->save();
        return redirect(route('admin.sejarah.index', [
                'edit'  => $Sejarah,
            ]));
    }
}
