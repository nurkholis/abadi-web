<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Album;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;

class AlbumController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        return view('webAdmin.album.album-index');
    }
    public function dataTable(Request $request, Album $Album)
    {   
        $data = $Album
            ->orderBy('album.id_album', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.album.edit', $data->id_album) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.album.delete', $data->id_album) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->addColumn('cover_album', function ($data) {
            return '
                <a href="' . asset('/public/images/album').'/'.$data->cover_album .'" target="_blank">
                    <img src=" ' . asset('/public/images/album').'/'.$data->cover_album .' " height="50">
                </a>
            ';
        })
        ->rawColumns(['cover_album','action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.album.album-form');
    }
    public function store(Request $request, Album $Album )
    {
        $this->validate($request, [
            'cover_album' => 'required',
            'nama_album' => 'unique:album|required'
        ]);
        
        $destinationPath = "public/images/album";
        $image = $request->file('cover_album');
        $cover_album = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $cover_album);

        $Album = $Album->create([
            'nama_album' => $request->nama_album,
            'cover_album' => $cover_album,
        ]);

        return redirect(route('admin.album.index'));
    }
    public function edit($id, Album $Album)
    {
        $Album = $Album->find($id);
        return view('webAdmin.album.album-form', [
            'edit'  => $Album,
            ]);
    }
    public function update(Request $request, $id, Album $Album)
    {
        $Album = $Album->find($id);
        if(!$request->cover_album == null ){
            $image_path = $destinationPath = "public/images/album/".$Album->cover_album;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/album";
            $image = $request->file('cover_album');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Album->cover_album = $image_name;
        }
        $Album->nama_album = $request->get('nama_album', $Album->nama_album);
        $Album->save();

        return redirect(route('admin.album.index'));
    }
    public function destroy($id, Album $Album)
    {
        $Album = $Album->find($id);
        $image_path = "public/images/album/".$Album->cover_album;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $Album = $Album->delete();
        return back();
    }
    
}
