<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Berita;
use Illuminate\Support\Facades\File;
use Auth;
use Config\Kholis as Helper;
use DataTables;

class beritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
        return view('webAdmin.berita.berita-index');
    }
    public function dataTable(Request $request, Berita $Berita)
    {   
        $data = $Berita
            ->join('admin', 'admin.id_admin', '=', 'berita.id_admin')
            ->orderBy('berita.id_berita', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.berita.edit', $data->id_berita) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.berita.delete', $data->id_berita) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->addColumn('tanggal', function($data){
            return Helper::tanggal($data->tanggal_berita);
        })
        ->addColumn('logo', function ($data) {
            return '
                <a href="' . asset('/public/images/berita').'/'.$data->cover .'" target="_blank">
                    <img src=" ' . asset('/public/images/berita').'/'.$data->cover .' " height="50">
                </a>
            ';
        })
        ->rawColumns(['logo','action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.berita.berita-form');
    }
    public function store(Request $request, Berita $Berita )
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d');

        $this->validate($request, [
            'cover_berita' => 'required',
            'judul_berita' => 'unique:berita|required'
        ]);
        
        $destinationPath = "public/images/berita/cover";
        $image = $request->file('cover_berita');
        $cover_berita = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $cover_berita);

        $Berita = $Berita->create([
            'judul_berita' => $request->judul_berita,
            'id_admin' => Auth::guard('admin')->user()->id_admin,
            'tanggal_berita' => $date,
            'cover_berita' => $cover_berita,
            'isi_berita' => $request->isi_berita,
        ]);

        return redirect(route('admin.berita.index'));
    }
    public function edit($id, berita $berita)
    {
        $berita = $berita->find($id);
        return view('webAdmin.berita.berita-form', [
            'edit'  => $berita,
            ]);
    }
    public function update(Request $request, $id, Berita $Berita)
    {
        $Berita = $Berita->find($id);
        if(!$request->cover_berita == null ){
            $image_path = $destinationPath = "public/images/berita/cover/".$Berita->cover_berita;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/berita/cover";
            $image = $request->file('cover_berita');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Berita->cover_berita = $image_name;
        }
        $Berita->judul_berita = $request->get('judul_berita', $Berita->judul_berita);
        $Berita->isi_berita = $request->get('isi_berita', $Berita->isi_berita);
        $Berita->save();

        return redirect(route('admin.berita.index'));
    }
    public function destroy($id, Berita $Berita)
    {
        $Berita = $Berita->find($id);
        $image_path = "public/images/berita/cover/".$Berita->cover_berita;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        $Berita = $Berita->delete();
        return back();
    }
    
}
