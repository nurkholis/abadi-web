<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Banner;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;
use Config\kholis as Helper;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {   
       return view('webAdmin.banner.banner-index');
    }
    public function dataTable(Banner $Banner)
    {
        $data = $Banner
            ->orderBy('banner.id_banner', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.banner.edit', $data->id_banner) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.banner.delete', $data->id_banner) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->addColumn('banner', function ($data) {
            return '
                <a href="' . asset('/public/images/banner').'/'.$data->banner .'" target="_blank">
                    <img src=" ' . asset('/public/images/banner').'/'.$data->banner .' " height="50">
                </a>
            ';
        })
        ->rawColumns(['banner','action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.banner.banner-form');
    }
    public function store(Request $request, Banner $Banner )
    {
        $this->validate($request, [
            'keterangan_banner' => 'required|min:3',
            'banner' => 'required',
        ]);
        
        $destinationPath = "public/images/banner";
        $image = $request->file('banner');
        $banner = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $banner);

        $Banner = $Banner->create([
            'id_bani' => 1,
            'banner' => $banner,
            'keterangan_banner' => $request->keterangan_banner,
        ]);

        return redirect(route('admin.banner.index'));
    }
    public function edit($id, Banner $Banner)
    {
        $Banner = $Banner->find($id);
        return view('webAdmin.banner.banner-form', [
            'edit'  => $Banner,
            ]);
    }
    public function update(Request $request, $id, Banner $Banner)
    {
        $Banner = $Banner->find($id);
        // jika edit gambar
        if(!$request->banner == null ){
            //menghapus gambar lama
            $image_path = $destinationPath = "public/images/banner/".$Banner->banner;     
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            //upload gambar baru
            $destinationPath = "public/images/banner";
            $image = $request->file('banner');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Banner->banner = $image_name;
        }
        $Banner->keterangan_banner = $request->get('keterangan_banner', $Banner->keterangan_banner);
        $Banner->save();

        return redirect(route('admin.banner.index'));
    }
    public function destroy($id, Banner $Banner)
    {
        $Banner = $Banner->find($id);

        $image_path = "public/images/banner/".$Banner->banner;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $Banner = $Banner->delete();
        return back();
    }
    
}
