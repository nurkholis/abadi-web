<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Laporan;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;
use Config\kholis as Helper;

class LaporanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {   
       return view('webAdmin.laporan.laporan-index');
    }
    public function dataTable(Laporan $Laporan)
    {
        $data = $Laporan
            ->orderBy('laporan.id_laporan', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.laporan.edit', $data->id_laporan) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.laporan.delete', $data->id_laporan) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->addColumn('laporan', function ($data) {
            return '
                <a href="' . asset('/public/images/laporan').'/'.$data->laporan .'" target="_blank">
                    <img src=" ' . asset('/public/images/laporan').'/'.$data->laporan .' " height="50">
                </a>
            ';
        })
        ->rawColumns(['laporan','action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.laporan.laporan-form');
    }
    public function store(Request $request, Laporan $Laporan )
    {
        $this->validate($request, [
            'keterangan_laporan' => 'required|min:3',
            'laporan' => 'required',
        ]);
        
        $destinationPath = "public/images/laporan";
        $image = $request->file('laporan');
        $laporan = time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath, $laporan);

        $Laporan = $Laporan->create([
            'id_bani' => 1,
            'laporan' => $laporan,
            'keterangan_laporan' => $request->keterangan_laporan,
        ]);

        return redirect(route('admin.laporan.index'));
    }
    public function edit($id, Laporan $Laporan)
    {
        $Laporan = $Laporan->find($id);
        return view('webAdmin.laporan.laporan-form', [
            'edit'  => $Laporan,
            ]);
    }
    public function update(Request $request, $id, Laporan $Laporan)
    {
        $Laporan = $Laporan->find($id);
        // jika edit gambar
        if(!$request->laporan == null ){
            //menghapus gambar lama
            $image_path = $destinationPath = "public/images/laporan/".$Laporan->laporan;     
            if (File::exists($image_path)) {
                //dd($image_path, 'file ditemukan');
                File::delete($image_path);
            }
            //upload gambar baru
            $destinationPath = "public/images/laporan";
            $image = $request->file('laporan');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Laporan->laporan = $image_name;
        }
        $Laporan->keterangan_laporan = $request->get('keterangan_laporan', $Laporan->keterangan_laporan);
        $Laporan->save();

        return redirect(route('admin.laporan.index'));
    }
    public function destroy($id, Laporan $Laporan)
    {
        $Laporan = $Laporan->find($id);

        $image_path = "public/images/laporan/".$Laporan->laporan;       
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $Laporan = $Laporan->delete();
        return back();
    }
    
}
