<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Artikel;
use Auth;
use DataTables;
use Config\Kholis as Helper;

class ArtikelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {  
       return view('webAdmin.artikel.artikel-index');
    }
    public function dataTable(Artikel $Artikel)
    {
        $data = $Artikel 
            ->orderBy('artikel.id_artikel', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.artikel.edit', $data->id_artikel) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.artikel.delete', $data->id_artikel) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->addColumn('tanggal', function($data){
            return Helper::tanggal($data->tanggal_artikel);
        })
        ->rawColumns(['logo','action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.artikel.artikel-form');
    }
    public function store(Request $request, Artikel $Artikel )
    {
        $this->validate($request, [
            'judul_artikel' => 'required|min:3',
        ]);
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d');
        $Artikel = $Artikel->create([
            'judul_artikel' => $request->judul_artikel,
            'tanggal_artikel' => $date,
            'penulis' => $request->penulis,
            'isi_artikel' => $request->isi_artikel,
        ]);

        return redirect(route('admin.artikel.index'));
    }
    public function edit($id, Artikel $Artikel)
    {
        $Artikel = $Artikel->find($id);
        return view('webAdmin.artikel.artikel-form', [
            'edit'  => $Artikel,
            ]);
    }
    public function update(Request $request, $id, Artikel $Artikel)
    {
        $Artikel = $Artikel->find($id);
        $Artikel->judul_artikel = $request->get('judul_artikel', $Artikel->judul_artikel);
        $Artikel->tanggal_artikel = $request->get('tanggal_artikel', $Artikel->tanggal_artikel);
        $Artikel->penulis = $request->get('penulis', $Artikel->penulis);
        $Artikel->isi_artikel = $request->get('isi_artikel', $Artikel->isi_artikel);
        $Artikel->save();

        return redirect(route('admin.artikel.index'));
    }
    public function destroy($id, Artikel $Artikel)
    {
        $Artikel = $Artikel->find($id)->delete();
        return back();
    }
    
}
