<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Anggota;
use App\Bani;
use App\Transformers\AnggotaTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Auth;
use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\DB;

class AnggotaController extends Controller
{
    public function get(Request $request, Anggota $Anggota, Bani $Bani, $id_bani){
        $bani = $Bani->find($request->id_bani);
        $param = [];
        $limit = 15;
        $data = $Anggota;
        if($request->id_anggota != "" ){
            $data = $data
                ->where('anggota.id_anggota', $request->id_anggota);
            $param['id_anggota']=$request->id_anggota;
        }
        if($request->search != "" ){
            $data = $data
                ->where('anggota.nama', 'LIKE', '%'. $request->search . '%');
            $param['search']=$request->search;
        }
        if($request->limit != "" ){
            $limit = $request->limit;
            $param['limit']=$request->limit;
        }
        $data = $data->paginate($limit)
            ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new AnggotaTransformer)
            ->includeAyah()
            ->includeIbu()
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
                'bani' => $bani
            ])
            ->toArray();
    }
}
