<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Album;
use App\Bani;
use App\Foto;
use App\Transformers\FotoTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class FotoController extends Controller
{
    public function get(Request $request, Foto $Foto, Album $Album, Bani $Bani, $id_bani){
        $album = $Album->find($request->id_album);
        $bani = $Bani->find(1);
        $param = [];
        $limit = 15;
        $data = $Foto
            ->join('album', 'album.id_album', '=', 'album.id_album')
            ->orderBy('foto.id_foto', 'desc');
            if($request->id_album != "" ){
                $data = $data
                    ->where('album.id_album', $request->id_album);
                $param['id_album']=$request->id_album;
            }
            if($request->id_foto != "" ){
                $data = $data
                    ->where('foto.id_foto', $request->id_foto);
                $param['id_foto']=$request->id_foto;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new FotoTransformer)
            ->includeAlbum()
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
                'album' => $album,
                'bani' => $bani,
            ])
            ->toArray();
    }
    
}
