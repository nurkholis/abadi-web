<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Banner;
use App\Bani;
use App\Transformers\BannerTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class BannerController extends Controller
{
    public function get(Request $request, Banner $Banner, Bani $Bani, $id_bani){
        $param = [];
        $limit = 15;
        $bani = $Bani->find($request->id_bani);
        $data = $Banner
            ->orderBy('banner.id_banner', 'desc');
            if($request->id_artikel != "" ){
                $data = $data
                    ->where('banner.id_banner', $request->id_banner);
                $param['id_banner']=$request->id_banner;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new BannerTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
                'bani' => $bani
            ])
            ->toArray();
    }
    
}
