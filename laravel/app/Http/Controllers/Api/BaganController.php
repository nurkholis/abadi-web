<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Anggota;

class BaganController extends Controller
{
    public function get(Request $request, $id_anggota){
        $array_anak = [];
        $array_cucu = [];
        $array_cicit = [];
        $anggota = Anggota::find($id_anggota);
        $anak = $anggota->where('id_ayah', '=', $id_anggota)
            ->orWhere('id_ibu', '=', $id_anggota)
            ->get();
        foreach ($anak as $a => $value) {
            $cucu = Anggota::where('id_ayah', '=', $value->id_anggota)
            ->orWhere('id_ibu', '=', $value->id_anggota)
            ->get();
            foreach ($cucu as $b => $value_cucu) {
                $cicit = Anggota::where('id_ayah', '=', $value_cucu->id_anggota)
                    ->orWhere('id_ibu', '=', $value_cucu->id_anggota)
                    ->get();
                foreach ($cicit as $b => $value_cicit) {
                    array_push($array_cicit, [
                        "id" => $value_cicit->id_anggota,
                        "name" => $value_cicit->nama,
                        "title" => "Generasi ke-" . $value_cicit->generasi,
                        "relationship" => [
                            "children_num" => "0"
                        ],
                        "children" => [],
                    ]);
                }
                array_push($array_cucu, [
                    "id" => $value_cucu->id_anggota,
                    "name" => $value_cucu->nama,
                    "title" => "Generasi ke-" . $value_cucu->generasi,
                    "relationship" => [
                        "children_num" => "0"
                    ],
                    "children" => $array_cicit,
                ]);
                $array_cicit = [];
            }
            array_push($array_anak, [
                "id" => $value->id_anggota,
                "name" => $value->nama,
                "title" => "Generasi ke-" . $value->generasi,
                "relationship" => [
                    "children_num" => "0"
                ],
                "children" => $array_cucu,
            ]);
            $array_cucu = [];
        }
        $data = [
            "id" => $anggota->id_anggota,
            "name" => $anggota->nama,
            "title" => "Generasi ke-" . $anggota->generasi,
            "relationship" => [
                "children_num" => "0"
            ],
            "children" => $array_anak,
        ];
        return response()->json($data);
    }
    
}
