<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Album;
use App\Bani;
use App\Transformers\AlbumTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class AlbumController extends Controller
{
    public function get(Request $request, Album $Album, Bani $Bani, $id_bani){
        $param = [];
        $limit = 15;
        $bani = $Bani->find(1);
        $data = $Album
            ->orderBy('album.id_album', 'desc');
            if($request->id_artikel != "" ){
                $data = $data
                    ->where('album.id_album', $request->id_album);
                $param['id_album']=$request->id_album;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new AlbumTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
                'bani' => $bani
            ])
            ->toArray();
    }
    
}
