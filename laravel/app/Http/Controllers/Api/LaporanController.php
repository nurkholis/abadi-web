<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Laporan;
use App\Bani;
use App\Transformers\LaporanTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class LaporanController extends Controller
{
    public function get(Request $request, Laporan $Laporan, Bani $Bani, $id_bani){
        $param = [];
        $limit = 15;
        $bani = $Bani->find($request->id_bani);
        $data = $Laporan
            ->orderBy('laporan.id_laporan', 'desc');
            if($request->id_artikel != "" ){
                $data = $data
                    ->where('laporan.id_laporan', $request->id_laporan);
                $param['id_laporan']=$request->id_laporan;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new LaporanTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
                'bani' => $bani
            ])
            ->toArray();
    }
    
}
