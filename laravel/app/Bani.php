<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Bani extends Authenticatable
{
    use Notifiable;

    protected $guard = 'bani';
    protected $table = 'bani';
    protected $primaryKey = 'id_bani';
    public $timestamps = false;
    protected $guarded = [];
}
