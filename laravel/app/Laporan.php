<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Laporan extends Authenticatable
{
    use Notifiable;

    protected $guard = 'laporan';
    protected $table = 'laporan';
    protected $primaryKey = 'id_laporan';
    public $timestamps = false;
    protected $guarded = [];
}
