<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Banner extends Authenticatable
{
    use Notifiable;

    protected $guard = 'banner';
    protected $table = 'banner';
    protected $primaryKey = 'id_banner';
    public $timestamps = false;
    protected $guarded = [];
}
