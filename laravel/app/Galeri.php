<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Galeri extends Authenticatable
{
    use Notifiable;

    protected $guard = 'galeri';
    protected $table = 'galeri';
    protected $primaryKey = 'id_galeri';
    public $timestamps = false;
    protected $guarded = [];
}
