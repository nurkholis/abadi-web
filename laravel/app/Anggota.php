<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Anggota extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;
    protected $guard = 'anggota';
    protected $table = 'anggota';
    protected $primaryKey = 'id_anggota';
    
    protected $guarded = [];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
