<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Sejarah extends Authenticatable
{
    use Notifiable;

    protected $guard = 'sejarah';
    protected $table = 'sejarah';
    protected $primaryKey = 'id_sejarah';
    public $timestamps = false;
    protected $guarded = [];
}
